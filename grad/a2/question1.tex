\section{Calculation Questions}

\subsection{Convexity}

\blu{Show that the following functions are convex, by only using one of the definitions of convexity (i.e., without using the ``operations that preserve convexity" or using convexity results stated in class)}:\footnote{That $C^0$ convex functions are below their chords, that $C^1$ convex functions are above their tangents, or that $C^2$ convex functions have a positive semidefinite Hessian.}
\enum{
\item L2-regularized weighted least squares: $f(w) = \half(Xw - y)^\top V(Xw-y)  + \frac \lambda 2 \norm{w}^2$.\\($V$ is a diagonal matrix with positive values on the diagonal).
    \ans{
    Taking the gradient of the function, we get
    \begin{align*}
        \nabla f(w) &= X^T V (Xw - y) + \lambda w \\
        &= (X^T V X + \lambda I) w - X^T V y.
    \end{align*}
    Since the matrix $V$ is diagonal, the Hessian is
    \begin{align*}
        \nabla^2 f(w) &= X^T V X + \lambda I.
    \end{align*}
    The diagonal matrix $V$ can be written as $V^{1/2} V^{1/2}$ where each entry of $V^{1/2}$ is the square root of that of $V$. 
    Therefore, $X^T V X$ is positive semidefinite because $z^T X^T V X z = \norm{V^{1/2} X z}^2 \geq 0$. Finally, since $\lambda >0$ and $I$ is positive definite, $\nabla^2 f(w)\succ 0$. So, it is convex.
    }
\item Poisson regression: $f(w) = -y^\top Xw + 1^\top v$ (where $v_i = \exp(w^\top x^i)$).
    \ans{
        Let $w = \theta a + (1-\theta) b$. We have $f(w)$ as
        \begin{align*}
            f(\theta a + (1-\theta) b) = -y^T X(\theta a + (1-\theta) b ) + \one^T exp[X(\theta a + (1-\theta) b)]. 
        \end{align*}
        For the first term, we have 
        \begin{align*}
            -y^T X(\theta a + (1-\theta) b )  = -\theta y^T X a - (1-\theta) y^T X b.
        \end{align*}
        For the second term, we have
        \begin{align*}
            \one^T \exp[X(\theta a + (1-\theta) b)] &\leq \one^T [\theta \exp(X a) + (1-\theta) \exp(Xb)]\\
            &= \theta \one^T \exp(X a) + (1-\theta) \one^T \exp(Xb) 
        \end{align*} because $\exp(\cdot)$ is a convex function (since $\frac{d^2}{dx^2} \exp(x) = \exp(x) > 0$).
        Putting the terms together, 
        \begin{align*}
            f(\theta a + (1-\theta) b) &\leq -\theta y^T X a - (1-\theta) y^T X b + \theta \one^T \exp(X a) + (1-\theta) \one^T \exp(Xb)  \\
             &= \theta [-y^T X a + \one^T \exp(Xa)] + (1-\theta)[-y^T X b + \one^T \exp(Xb)] \\
             &= \theta f(a) + (1-\theta)f(b).
        \end{align*}
        Therefore, it is a convex function.
    }
\item Weighted infinity-norm: $f(w) = \max_{j \in \{1,2,\dots,d\}}L_j|w_j|$.\\
    Hint: Max and aboluste value are not differentiable in general, so you cannot use the Hessian for this question. $\abs{x}$
\ans{
    Let $w = \theta a + (1-\theta) b$, we have 
    \begin{align*}
        f(\theta a + (1-\theta)b) &= \max_{j} L_j \abs{\theta a_j + (1-\theta)b_j}\\ 
        &\leq \max_j L_j \left(\abs{\theta a_j} + \abs{(1-\theta)b_j} \right)\\ 
        &= \max_j L_j \left(\theta \abs{a_j} + (1-\theta)\abs{b_j} \right)\\ 
        &\leq \max_j \theta L_j \abs{a_j} + \max_j (1-\theta) L_j \abs{b_j}\\
        &= \theta f(a) + (1-\theta) f(b).
    \end{align*}
    Therefore, it is a convex function.
}
}

\blu{Show that the following functions are convex (you can use results from class and operations that preserve convexity if they help)}:
\enum{
\setcounter{enumi}{3}
\item Regularized regression with arbitrary $p$-norm and weighted $q$-norm: $f(w) = \norm{Xw - y}_p + \lambda\norm{Aw}_q$.
    \ans{
        From the class, we know that norms are convex.
        For the first term, because $Xw-y$ is linear and $\norm{\cdot}_p$ is a norm, their composition is convex. 
        For the second term, since $Aw$ is a linear function, $\norm{\cdot}_q$ is a norm, the term $\norm{Aw}_q$ is convex. Multiplying a convex function with a positive contant does not change convexity, so $\lambda \norm{Aw}_q$ is convex.
        Finally, because $\norm{Xw-y}_p$ and $\lambda \norm{Aw}_q$ are both convex, their sum is convex too.
    }
\item Support vector regression: $f(w) = \sum_{i=1}^N\max\{0, |w^\top x_i - y_i| - \epsilon\} + \frac{\lambda}{2}\norm{w}_2^2$.
    \ans{
        We know that norms are convex.
        For the left term, $\abs{w^T x_i - y_i}$ is a linear function going through a norm so it is convex. Since shifting a function does not change its shape, $\abs{w^T x_i - y_i} - \epsilon$ is convex. Because $0$ is a linear function, $\max\{0, \abs{w^T x_i - y_i} - \epsilon\}$ is a max of two convex functions, so it is convex. The summation term $\sum_{i}$ adding up convex functions, so the left term is convex.
        For the right term, it is a squared norm, so it is convex. Multiplying a convex function with a positve constant preserves convexity, so $\frac{\lambda}{2} \norm{w}^2_2$ is convex. Finally, the sum of the left term and the right term should be convex because they are both convex functions. Therefore, $f(w)$ is a convex function.
    }
\item Indicator function for linear constraints: $f(w) = \begin{cases}0 & \text{if $Aw \leq b$}\\\infty & \text{otherwise}\end{cases}$.
    \ans{
        Let the set $C = \{ w\mid A w \leq b\}$, the indicator function can be written as 
        \begin{align*}
            f(w) = \begin{cases}0 & \text{if $w \in C$}\\
            \infty & \text{otherwise}\end{cases}.
        \end{align*}
        Since all half spaces are convex and $C$ is a halfspace, $C$ is a convex set. 
        On the domain (i.e. $C$) of the function, take two arbitrary points $a \in C$ and $b \in C$. 
        Because $a$ and $b$ are in $C$, we have
        \begin{align*}
        	f(a) &= 0 \\
        	f(b) &= 0.
        \end{align*}
        From the results above, we can see that 
        \begin{align*}
            \theta f(a) + (1-\theta) f(b) & = \theta 0 + (1-\theta) 0 = 0.
        \end{align*}
        Because the domain $C$ is a convex set, the convex combination of $a$ and $b$ will stay in $C$. In other words, $\forall \theta \in [0, 1], \ \theta a + (1-\theta) b \in C.$
        Therefore, 
        \begin{align*}
            f(\theta a + (1 - \theta) b) &= 0 
        \end{align*}
        Finally, 
        \begin{align*}
            0 = f(\theta a + (1 - \theta) b) &\leq \theta f(a) + (1-\theta) f(b) = 0.
        \end{align*}
        So the indicator function with linear constraint is a convex function.
    }
}



\subsection{Convergence of Gradient Descent}

For these questions it will be helpful to use the ``convexity inequalities'' notes posted on the webpage.

\enum{
\item In class we showed that if $\nabla f$ is $L$-Lipschitz continuous and $f$ is bounded below then with a step-size of $1/L$ gradient descent is guaranteed to have found a $w^k$ with $\norm{\nabla f(w^k)}^2 \leq \epsilon$ after $t = \bigO(1/\epsilon)$ iterations. Suppose that a more-clever algorithm exists which, on iteration $t$, is guaranteed to have found a $w^k$ satisfying $\norm{\nabla f(w^k)}^2 \leq 2L(f(w^0) - f^*)/t^{4/3}$. \blu{How many iterations of this algorithm would we need to find a $w^k$ with $\norm{\nabla f(w^k)}^2 \leq \epsilon$?}
\ans{Suppose we have devised an algorithm for which, on iteration $t$, we are guaranteed to have found a $w^k$ satisfying $\norm{\nabla f(w^k)}^2 \leq \epsilon$. In other words: $$\min_{j\in\{0, \ldots, t-1\}} \{\norm{\nabla f(w^j)}^2\} \leq \frac{2L(f(w^0)-f^*)}{t^{4/3}}$$ Bounding this above by $\epsilon$ gives
$$\frac{2L(f(w^0)-f^*)}{t^{4/3}} \leq \epsilon$$
so 
$$t^{4/3} \geq \frac{2L(f(w^0)-f^*)}{\epsilon}$$
and thus 
$$t \geq \left(\frac{2L(f(w^0)-f^*)}{\epsilon}\right)^{3/4} $$ or $t = \bigO((1/\epsilon)^{3/4})$}
\item In practice we typically don't know $L$. A common strategy in this setting is to start with some small guess $L^0$ that we know is smaller than the true $L$ (usually we take $L^0=1$). On each iteration $k$, we initialize with $L^k = L^{k-1}$ and we check the inequality
\[
f\left(w^k - \frac{1}{L^k}\nabla f(w^k)\right) \leq f(w^k) - \frac{1}{2L^k}\norm{\nabla f(w^k)}^2.
\]
If this is not satisfied, we double $L^k$ and test it again. This continues until we have an $L^k$ satisfying the inequality, and then we take the step. \blu{Show that gradient descent with $\alpha_k = 1/L^k$ defined in this way has a linear convergence rate of
\[
f(w^k) - f(w^*) \leq \left(1 - \frac{\mu}{2L}\right)^k[f(w^0) - f(w^*)],
\]
\red{if $\nabla f$ is $L$-Lipschitz continuousn and $f$ is $\mu$-strongly convex.}\\
} Hint: if a function is $L$-Lipschitz continuous that it is also $L'$-Lipschitz continuous for any $L' \geq L$.
\ans{
At each step, we have
$$f\left(w^k - \frac{1}{L^k}\nabla f(w^k)\right) = f(w^{k+1}) \leq f(w^k) - \frac{1}{2L^k}\norm{\nabla f(w^k)}^2$$
Note that when $L^k$ is defined as described in the problem statement we have $L^k < 2L$, so 
$$f(w^k) - \frac{1}{2L^k}\norm{\nabla f(w^k)}^2 \leq f(w^k) - \frac{1}{4L}\norm{\nabla f(w^k)}^2$$
and thus 
$$f(w^{k+1}) \leq f(w^k) - \frac{1}{4L}\norm{\nabla f(w^k)}^2.$$
Since $f$ is $\mu$-strongly convex, we can apply the PL inequality giving
$$f(w^{k+1}) \leq f(w^k) - \frac{\mu}{2L}(f(w^k) - f^*).$$
Subtracting $f^*$ from both sides and factoring gives
$$f(w^{k+1}) - f^* \leq \left(1 - \frac{\mu }{2L}\right)(f(w^k) - f^*)$$
Applying this recursively yields
$$f(w^{k+1}) - f^* \leq \left(1 - \frac{\mu }{2L}\right)^k(f(w^0) - f^*)$$
which is what we set out to show.
}
\item Suppose that, in the previous question, we initialized with $L^k = \red{\half}L^{k-1}$. \blu{Describe a setting where this could work much better}.
\ans{Halving $L^k$ on each iteration corresponds to doubling the step size, which would be useful in situations where the gradient is dying off exponentially. One such example is trying to find a value of $x$ for which $e^{-x} < \epsilon$ for some $\epsilon > 0$.}
\item In class we showed that if $\nabla f$ is $L$-Lipschitz continuous and $f$ is strongly-convex, then with a step-size of $\alpha_k = 1/L$ gradient descent has a convergence rate of 
\[
f(w^k) - f(w^*) = O(\rho^k).
\]
\blu{Show that under these assumptions that a convergence rate of $O(\rho^k)$ in terms of the function values implies that the iterations have a convergence rate of
\[
\norm{w^k - w^*} = O(\rho^{k/2}).
\]}
\ans{Since $f$ is $\mu$-strongly convex, we have
$$f(v) \geq f(w) + \nabla f(w)^T(v-w) + \frac{\mu }{2}\norm{v-w}^2$$
for all $v,w$. Plugging in $w=w^*, v = w^k$:
$$f(w^k) \geq f(w^*) + \nabla f(w^*)^T(w^k-w^*) + \frac{\mu }{2}\norm{w^k-w^*}^2$$
Since $\nabla f(w^*) = 0$, this gives
$$f(w^k) - f(w^*) \geq \frac{\mu }{2}\norm{w^k-w^*}^2$$
Using the linear convergence result for gradient descent with PL functions:
$$\frac{\mu }{2}\norm{w^k - w^*}^2 \leq f(w^k) - f^* \leq \left(1 - \frac{\mu }{L}\right)^k(f(w^0)-f^*)$$
so 
$$\norm{w^k-w^*} \leq \left(1 - \frac{\mu }{L}\right)^{k/2} \sqrt{\frac{2}{\mu } (f(w^0) - f^*)}$$
so $\norm{w^k-w^*} = \bigO(\rho^{k/2})$.
}
}




\subsection{Beyond Gradient Descent}


\enum{
\item We can write the proximal-gradient update as
\begin{align*}
w^{k+\half} & = w^k - \alpha_k \nabla f(w^k)\\
w^{k+1} &= \argmin{v\in\R^d}\left\{\frac{1}{2}\norm{v -w^{k+\half}}^2 + \alpha_kr(v)\right\}.
\end{align*}
\blu{Show that this is equivalent to setting
\[
w^{k+1} \in \argmin{v\in\R^d} \left\{ f(w^k) + \nabla f(w^k)^\top (v-w^k) + \frac{1}{2\alpha_k}\norm{v-w^k}^2  +r(v)\right\}.
\]
}
\ans{
    Putting $w^{k+\half}$ into the second equation, we have 
    \begin{align*}
        w^{k+1} &= \argmin{v\in\R^d}\left\{\frac{1}{2}\norm{v -(w^k - \alpha_k \nabla f(w^k))}^2 + \alpha_kr(v)\right\}\\
        &= \argmin{v\in\R^d}\left\{\frac{1}{2}\norm{(v - w^k) + \alpha_k \nabla f(w^k)}^2 + \alpha_kr(v)\right\}\\
        &= \argmin{v\in\R^d}\left\{\frac{1}{2}
        \left( \norm{v - w^k}^2 + \alpha_k^2 \norm{\nabla f(w^k)}^2
        + 2 \alpha_k \nabla f(w^k)^T (v- w^k)
        \right)
        + \alpha_kr(v)\right\}\\
        &= \argmin{v\in\R^d}\left\{
            \frac{1}{2}\norm{v - w^k}^2 + \frac{\alpha_k^2}{2} \norm{\nabla f(w^k)}^2
        + \alpha_k \nabla f(w^k)^T (v- w^k)
        + \alpha_kr(v)\right\}\\
        &= \argmin{v\in\R^d}\left\{
            \frac{1}{2 \alpha_k}\norm{v - w^k}^2 + \frac{\alpha_k}{2} \norm{\nabla f(w^k)}^2
        + \nabla f(w^k)^T (v- w^k)
        + r(v)\right\}.
    \end{align*}
    Since the optimization is with respect to $v$ we could add and subtract any constant. Finally, 
    \begin{align*}
        w^{k+1} &= \argmin{v\in\R^d}\left\{
            \frac{1}{2 \alpha_k}\norm{v - w^k}^2 + \nabla f(w^k)^T (v- w^k)
        + r(v)\right\}\\
        &= \argmin{v\in\R^d}\left\{
            \frac{1}{2 \alpha_k}\norm{v - w^k}^2   
            + \nabla f(w^k)^T (v- w^k) + f(w^k)
            + r(v)\right\}\\
        &= \argmin{v\in\R^d} \left\{ f(w^k) + \nabla f(w^k)^\top (v-w^k) + \frac{1}{2\alpha_k}\norm{v-w^k}^2  +r(v)\right\}.
    \end{align*}
}
\item The ``sum'' version of multi-class SVMs uses an objective of the form
\[
f(W) = \sum_{i=1}^n \sum_{c \neq y^i}[1 - w_{y^i}^\top x^i + w_c^\top x^i]^+ + \frac \lambda 2 \norm{W}_F^2,
\]
where $[\gamma]^+$ sets negative values to zero (and you can use $k$ as the number of classes so the inner loop is over $(k-1)$ elements). \blu{Derive the sub-differential of this objetive}.
\ans{
    For the regularizer, we could directly take the gradient and get $\frac{\lambda}{2} 2 W = \lambda W$. 
    Thus, the sub-differential with respect to the element $W_{p, q}$ can be written as 
    \begin{align*}
        \partial_{W_{p, q}} f(W) = \sum_{i=1}^n \sum_{c \neq y^i} g^i_{p, q} + \lambda W_{p, q}.
    \end{align*} where
    \begin{align*}
        g^i_{p, q} &= \partial_{W_{p, q}} [1-w^\top_{y^i} x^i + w^\top_c x^i]^+\\
        &= \partial_{W_{p, q}} \max\{0, 1-w^\top_{y^i} x^i + w^\top_c x^i\}.
    \end{align*}
    Note that $\sum_i \sum_c$ is the Minkovski sum of each sub-differential.
    Lets perform some case analysis on $g^i_{p, q}$. For a given $i, c, p, q$, the sub-differential $g^i_{p, q}$ takes different values: 
    \begin{align*}
        g^i_{p, q} = 
        \begin{cases}
            \{0\} & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i < 0$}\\
            \{-x_q^i\} & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i > 0, p = y^i$}\\
            \{x_q^i\} & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i > 0, p = c$}\\
            \{0\} & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i > 0, p \neq c, p \neq y^i$}\\
            -x^i_q \cdot [0, 1] & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i = 0, p = y^i$}\\
            x^i_q \cdot [0, 1] & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i = 0, p = c$}\\
            \{0\} & \text{if $1-w^\top_{y^i} x^i + w^\top_c x^i = 0, p \neq c, p \neq y^i$}
        \end{cases}
    \end{align*}
    where $k\cdot [0, 1]$ denotes the interval $[0, k]$ if $k \geq 0$ and $[k, 0]$ if $k < 0$.
}
\item In some situations it might be hard to accurately compute the elements of the gradient, but we might have access to the sign of the gradient (this can also be useful in distributed settings where communicating one bit for each element of the gradient is cheaper than communicating a floating point number for each gradient element). 
Consider an $f$ that is bounded below and where $\nabla f$ is Lipschitz continuous in the $\infty$-norm, meaning that
\[
f(v) \leq f(u) + \nabla f(u)^\top (v-u) + \frac{L_\infty}{2}\norm{v-u}_\infty^2,
\]
for all $v$ and $w$ and some $L_\infty$. 
For this setting, consider a sign-based gradient descent algorithm of the form
\[
w^{k+1} = w^k - \frac{\norm{\nabla f(w^k)}_1}{L_\infty}\text{sign}(\nabla f(w^k)),
\]
where we define the sign function element-wise as
\[
\text{sign}(w_j) = \begin{cases}+1 & w_j > 0\\0 & w_j =0\\-1 & w_j < 0\end{cases},
\]
\blu{Show that this sign-based gradient descent algorithm finds a $w^k$ satisfying \red{$\norm{\nabla f(w^k)}^2 \leq \epsilon$} after $t = O(1/\epsilon)$ iterations.}
\ans{
    Using the lemma above, we have
    \begin{align*}
        f(w^{k+1}) \leq f(w^k) + \nabla f(w^k)^\top (w^{k+1} - w^k) + \frac{L_\infty}{2} \norm{w^{k+1} - w^k}_\infty^2.
    \end{align*}
    Putting the second equation 
    \begin{align*}
    w^{k+1} - w^k = - \frac{\norm{\nabla f(w^k)}_1}{L_\infty}\text{sign}(\nabla f(w^k)),
    \end{align*}
    into the lemma, we have
    \begin{align*}
        f(w^{k+1}) &\leq f(w^k) + \nabla f(w^k)^\top \left(- \frac{\norm{\nabla f(w^k)}_1}{L_\infty}\text{sign}(\nabla f(w^k))\right) + \frac{L_\infty}{2} \norm{- \frac{\norm{\nabla f(w^k)}_1}{L_\infty}\text{sign}(\nabla f(w^k))}_\infty^2 \\
        &= f(w^k) - \frac{\norm{\nabla f(w^k)}_1}{L_\infty}\nabla f(w^k)^\top \text{sign}(\nabla f(w^k)) + \frac{\norm{\nabla f(w^k)}_1^2}{2 L_\infty} \norm{\text{sign}(\nabla f(w^k))}_\infty^2 \\
        &= f(w^k) - \frac{\norm{\nabla f(w^k)}_1}{L_\infty}\norm{\nabla f(w^k)}_1 + \frac{\norm{\nabla f(w^k)}_1^2}{2 L_\infty} \cdot 1 \\
        &= f(w^k) - \frac{\norm{\nabla f(w^k)}_1^2}{L_\infty} + \frac{\norm{\nabla f(w^k)}_1^2}{2 L_\infty}  \\
        &= f(w^k) - \frac{\norm{\nabla f(w^k)}_1^2}{2 L_\infty}. 
    \end{align*}
    Summing up on left and right: 
    \begin{align*}
        \sum_{k = 0}^{t-1} f(w^{k+1}) - f(w^k)&\leq \sum_{k = 0}^{t-1} -\frac{\norm{\nabla f(w^k)}_1^2}{2 L_\infty}\\
        f(w^{t}) - f(w^0)&\leq -\frac{1}{2 L_\infty} \sum_{k = 0}^{t-1} \norm{\nabla f(w^k)}_1^2\\
        &\leq -\frac{1}{2 L_\infty} t \min_j \norm{\nabla f(w^j)}_1^2.
    \end{align*}
    Notice that $f^* \leq f(w^t)$ by definition so $f^* - f(w^0) \leq f(w^t) - f(w^0)$. Therefore, 
    \begin{align*}
        f^* - f(w^0) \leq  -\frac{1}{2 L_\infty} t \min_j \norm{\nabla f(w^j)}_1^2.
    \end{align*}
    Rearrange the term and bound the gradient by $\epsilon$, 
    \begin{align*}
        -\frac{2L_\infty(f^* - f(w^0))}{t} &\geq    \min_j \norm{\nabla f(w^j)}_1^2 \\
        \min_j \norm{\nabla f(w^j)}_1^2 &\leq \frac{2L_\infty(f(w^0) - f^*)}{t} \leq \epsilon\\
        t &\geq \frac{2L_\infty (f(w^0) - f^*)}{\epsilon}
    \end{align*}
    Finally, 
    \begin{align*}
        t \geq \frac{2L_\infty (f(w^0) - f^*)}{\epsilon} = \bigO(1/\epsilon).
    \end{align*}
}}
