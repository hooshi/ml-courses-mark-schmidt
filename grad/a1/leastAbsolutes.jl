import MathProgBase
import Clp

function leastAbsolutes(X,y)

    # Setup the linear Program
	n = size(X,1)
	Z = [ones(n,1) X]
	d = size(Z,2)

    c  = zeros(n+d,1)
    c[1:n] .= 1 

    A  = zeros(2*n,n+d)
    #
    A[1:n,1:n] = Eye(n)
    A[(n+1):(2*n),1:n] = Eye(n)
    #
    A[1:n,(n+1):(n+d)] = Z
    A[(n+1):(2*n),(n+1):(n+d)] = -Z

    v  = zeros(2*n,1)
    v[1:n] = y 
    v[(n+1):(2*n)] = -y 


    # Solve the linprog
    solution = MathProgBase.linprog(c[:], A, v[:], Inf, -Inf, Inf, Clp.ClpSolver())
    @printf("Lingprog solved\n")
    @printf("objective = %.10g \n", solution.objval)
    @printf("status = %s \n", solution.status)
    
    w = solution.sol[(n+1):(n+d)]

	# Make linear prediction function
	predict(Xtilde) = [ones(size(Xtilde,1),1) Xtilde]*w

	# Return model
	return LinearModel(predict,w)
end

