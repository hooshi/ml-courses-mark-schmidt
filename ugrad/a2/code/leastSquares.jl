include("misc.jl")

##
## Construct a polynomial of order p out of a vector X
##
function constructPolynomialBasis(XX, p)
    n_rows = size(XX,1)
    Xaug = zeros(n_rows, 0)

    newcol = ones(n_rows,1)
    for i=0:p
        Xaug = hcat(Xaug, newcol)
        newcol = newcol .* XX
    end

    return Xaug
end

function constructRBFBasis(XX, sigma, p)
    n_rows = size(XX,1)
    sigma = Float64(sigma)
    Xaug = zeros(n_rows, 0)

    newcol = exp.( XX.*XX /sigma/sigma )
    for i=0:p
        Xaug = hcat(Xaug, newcol)
        newcol = newcol .* XX / sigma * sqrt(factorial(p+1)) / sqrt(factorial(p))
    end

    return Xaug
end

function constructFourierBasis(XX, p, n, L)
    n_rows = size(XX,1)
    Xaug = zeros(n_rows, 0)

    for i=1:n
        newcol = sin.(i*3.14159265359/Float64(L) * XX)
        Xaug = hcat(Xaug, newcol)

        newcol = cos.(i*3.14159265359/Float64(L) * XX)
        Xaug = hcat(Xaug, newcol)
    end

    XXp = ones(n_rows, 1)
    for i=0:p
        Xaug = hcat(Xaug, XXp)
        XXp = XXp .* XX
    end

    return Xaug
end

##
## Least squares with nothing
##
function leastSquares(X,y)

    # Find regression weights minimizing squared error
    w = (X'*X)\(X'*y)

    # Make linear prediction function
    predict(Xhat) = Xhat*w

    # Return model
    return GenericModel(predict)
end

##
## Least squares with weights
##
function weightedLeastSquares(X,y, v)

    ## add the weights
    sqrt_v = sqrt.(v)
    y = y .* sqrt_v
    X = diagm(sqrt_v) * X 
    
    # Find regression weights minimizing squared error
    w = (X'*X)\(X'*y)

    # Make linear prediction function
    predict(Xhat) = Xhat*w

    # Return model
    return GenericModel(predict)
end

##
## Least squares with polynomial feature augmentation
##
function leastSquaresBasis(X,y1; p=1)
    # Add a new feature for the intercept
    Xaug = constructPolynomialBasis(X, p) 
    
    # Find regression weights minimizing squared error
    w = (Xaug'*Xaug)\(Xaug'*y)

    # Make linear prediction function
    predict(Xhat) = constructPolynomialBasis(Xhat, p) *w

    # Return model
    return GenericModel(predict)
end

##
## Least squares with the intercept term
##
function leastSquaresBias(X,y)
    return leastSquaresBasis(X,y,p=1)
end

##
## Least squares with limited rbf
##
function leastSquaresRBF(X,y,sigma,p)
    # Add a new feature for the intercept
    Xaug = constructRBFBasis(X, sigma, p) 
    
    # Find regression weights minimizing squared error
    w = (Xaug'*Xaug)\(Xaug'*y)

    # Make linear prediction function
    predict(Xhat) = constructRBFBasis(Xhat, sigma, p) *w

    # Return model
    return GenericModel(predict)
end

##
## Least squares with Fourier
##
function leastSquaresFourier(X,y,p, n, L)
    # Add a new feature for the intercept
    Xaug = constructFourierBasis(X, p, n, L) 
    
    # Find regression weights minimizing squared error
    w = (Xaug'*Xaug)\(Xaug'*y)

    # Make linear prediction function
    predict(Xhat) = constructFourierBasis(Xhat, p, n, L) *w

    # Return model
    return GenericModel(predict)
end


##
## A factory for least square types
##
function leastSquaresFactory(XX, yy, typename)

    if( typename == "nobias" )
        return leastSquares(XX, yy)
    elseif(typename == "bias" )
        return leastSquaresBias(XX, yy)
    elseif(typename == "poly" )
        p = readParam(ARGS, "-p", Int, 1);
        return leastSquaresBasis(XX, yy, p=p)
    elseif(typename == "rbf" )
        p = readParam(ARGS, "-p", Int, 1);
        sigma = readParam(ARGS, "-sigma", Float64, 1.);
        return leastSquaresRBF(XX, yy, sigma, p)
    elseif(typename == "fourier" )
        p = readParam(ARGS, "-p", Int, 1);
        L = readParam(ARGS, "-L", Float64, 10);
        n = readParam(ARGS, "-n", Int, 1);
        return leastSquaresFourier(XX, yy, p, n, L)
    else
        error(@sprintf("LSQ type %s not allowed", typename) )
    end
    
end
