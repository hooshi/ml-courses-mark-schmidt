#!/home/hooshi/bin/julia

# Load X and y variable
using JLD
X = load("citiesSmall.jld","X")
y = load("citiesSmall.jld","y")
Xtest = load("citiesSmall.jld","Xtest")
ytest = load("citiesSmall.jld","ytest")


# Read K from input
n_args = size(ARGS,1)
if ( n_args == 0 )
    k = 100
else
    k = parse(Int,ARGS[1])
end
@printf "k=%d\n" k

# Fit a KNN classifier
include("knn.jl")
model = knn(X,y,k)

# Evaluate training error
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@printf("Train Error with %d-nearest neighbours: %.3f\n",k,trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error with %d-nearest neighbours: %.3f\n",k,testError)

# Plot classifier
include("plot2Dclassifier.jl")
plot2Dclassifier(X,y,model)
plot2Dclassifier(Xtest,ytest,model)

show()
