# Load X and y variable
import JLD
import Printf
import PyPlot
include("../a1.jl")
using Parameters

#include("leastSquares.jl")

data = JLD.load("nonLinear.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])
X = convert(a1.MatFloat, X) 
# test = a1.generateRBFBasis(convert(a1.MatFloat, X),0.)


r = convert(a1.VecFloat, range(-2,stop=2,length=1000))
Printf.@printf("shayan \n")

g = a1.evalRBF(r.*r, 0.5)
PyPlot.plot(r, g)
# PyPlot.show()

basis = a1.generateRBFBasis(X,X,1.)

model = a1.leastSquaresRBFL2(X,y[:],1.,1.)

# leastSquaresRBFL2
