#!/usr/bin/env julia

using JLD
include("logReg.jl")
include("misc.jl")
include("plot2Dclassifier.jl")

##
## Load X and y variable
##
data = load("multiData.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

##
## Data is already roughly standardized, but let's add bias
##

## train
n = size(X,1)
X = [ones(n,1) X]

## test
t = size(Xtest,1)
Xtest = [ones(t,1) Xtest]

##
## Fit the classifier
##
model_name = readParam(ARGS, "-model", String, "ova")
model = nothing

@show(ARGS)
if(model_name == "ova")
    model = logRegOnevsAll(X,y)
    @printf("Model is one vs all\n")
elseif (model_name == "softmax")
    model = logRegSoftmax(X,y)
    @printf("Model is softmax\n")
else
    @printf("Model name %s not recognized\n", model_name)
    @assert(false)
end

##
## Compute training and validation error
##
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@show(trainError)
yhat = model.predict(Xtest)
validError = mean(yhat .!= ytest)
@show(validError)

##
## Plot results
##
k = maximum(y)
plot2Dclassifier(X,y,model,Xtest=Xtest,ytest=ytest,biasIncluded=true,k=5,figure_index=1)
plt.show()
