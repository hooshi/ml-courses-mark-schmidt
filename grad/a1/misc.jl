const Float = Float64
const Int = Int64
const VecFloat = Array{Float, 1}
const VecInt = Array{Int, 1}
const MatFloat = Array{Float, 2}
const MatInt = Array{Int, 2}

mutable struct LinearModel
	predict # Funcntion that makes predictions
	w # Weight vector
end

# Return squared Euclidean distance all pairs of rows in X1 and X2
function distancesSquared(X1,X2)
	(n,d) = size(X1)
	(t,d2) = size(X2)
	@assert(d==d2)
	return X1.^2*ones(d,t) + ones(n,d)*(X2').^2 - 2X1*X2'
end

### A function to compute the gradient numerically
function numGrad(func,x)
	n = length(x);
	delta = 2*sqrt(1e-12)*(1+norm(x));
	g = zeros(n);
	e_i = zeros(n)
	for i = 1:n
		e_i[i] = 1;
		(fxp,) = func(x + delta*e_i)
		(fxm,) = func(x - delta*e_i)
		g[i] = (fxp - fxm)/2delta;
		e_i[i] = 0
	end
	return g
end


### Check if number is a real-finite number
function isfinitereal(x)
	return (imag(x) == 0) & (!isnan(x)) & (!isinf(x))
end

## Stupid Julia
function Eye(n::Int)
    return MatFloat(LinearAlgebra.I,n,n)
end

##
## Testing the derivatives this way is more robust to round off errors
## Thanks to Eldad Haber
function derivTest(
    state::VecFloat,
    delta::VecFloat,
    objFun::Any,
    gradFun::Any;
    numHalving::Int = 5,
    h::Float = 0.5)    
    
    diff0 = 0
    diff1 = 0

    Printf.@printf("%16s %16s %16s\n", "n", "DIFF0", "DIFF1")

    value0 = objFun(state);
    for ih in 1:numHalving
        h = h / 2. ;

        hdelta = h*delta;
        value1exact = objFun(state+ hdelta);

        eval(state);
        value1lin = value0 + gradFun(hdelta);

        diff0new = norm(value0 - value1exact );
        diff1new = norm(value1lin - value1exact);
        Printf.@printf("%16d %16.4e(%4.0f) %16.4e(%4.0f)  \n", ih, diff0new, diff0 / diff0new, diff1new, diff1 / diff1new);
        diff0 = diff0new;
        diff1 = diff1new;
    end   
end
