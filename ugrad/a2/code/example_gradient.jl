#!/usr/bin/env julia

# Load X and y variable
import PyCall
PyCall.@pyimport matplotlib.pyplot as plt
import JLD
data = JLD.load("outliersData.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

# Fit a least squares model
#include("leastSquaresGradient.jl")
include("robustRegression.jl")
model = robustRegression(X,y)

# Evaluate training error
yhat = model.predict(X)
#trainError = mean((yhat - y).^2)
#@printf("Squared train Error with least squares: %.3f\n",trainError)
trainError = mean(abs.(yhat - ytest))
@printf("L_1 norm of train Error with least L_1: %.3f\n",trainError)

# Evaluate test error
yhat = model.predict(Xtest)
#testError = mean((yhat - ytest).^2)
#@printf("Squared test Error with least squares: %.3f\n",testError)
testError = mean(abs.(yhat - ytest))
@printf("L_1 norm of test Error with least L_1: %.3f\n",testError)

plt.figure()
plt.plot(X,y,"b.")
Xhat = minimum(X):.01:maximum(X)
yhat = model.predict(Xhat)
plt.plot(Xhat,yhat,"g")
plt.xlabel("\$x\$")
plt.ylabel("\$y\$")
plt.savefig("gradient-fitting.pdf", bbox_inches="tight")
plt.show()
