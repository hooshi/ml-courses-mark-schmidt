#!/home/hooshi/bin/julia

include("misc.jl")

function write_data(data, stream)
    data_size = size(data)
    @printf "Data size: %s\n" data_size
    
    for i = 1:data_size[1]
        for j = 1:data_size[2]
            @printf(stream, "%10s ", data[i,j])
        end
        @printf(stream, "\n")
    end
end


##-----------------------------------------------
##-----------------------------------------------

##
## Load the data
##
data = readcsv("fluTrends.csv")
XX = real( data[2:end,:] )
n_sample = size(XX,1)
n_feature = size(XX,2)

##
## Write the data to file
##
stream = open("out/fluTrends.txt", "w")
write_data(data, stream)
close(stream)

##
## Loop over the features and find their:
##   min, max, mean, median, and mode
##
@printf("%10s %10s %10s %10s %10s %10s \n",
        "feature #", "min", "max", "mean", "median", "mode")
for j=1:n_feature
    yy = XX[:,j]
    min_val = minimum(yy)
    max_val = maximum(yy)
    mean_val = mean(yy)
    mode_val = mode(yy)
    median_val = median(yy)

@printf("%10s %10.5f %10.5f %10.5f %10.5f %10.3f \n",
        j, min_val, max_val, mean_val, median_val, mode_val)
end

yy = reshape(XX, n_feature*n_sample)
min_val = minimum(yy)
max_val = maximum(yy)
mean_val = mean(yy)
mode_val = mode(yy)
median_val = median(yy)
@printf("%10s %10.5f %10.5f %10.5f %10.5f %10.3f \n",
        "All", min_val, max_val, mean_val, median_val, mode_val)
