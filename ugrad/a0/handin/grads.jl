#!/usr/bin/julia

### -----------------------------------------------------------------------
###  Assumptions
###
###  x is a column vector The derivative matrix is created
###  such that Deriv_{ij} = df_i/dx_j.  Therefore, the gradient of a
###  scalar function will always be a row vector.
### -----------------------------------------------------------------------
function to_col(x)
    return x[:]
end

function to_row(x)
    return transpose(x[:])
end

### -----------------------------------------------------------------------
###                     An example to get you started:
### -----------------------------------------------------------------------

# Function
func0(x) = sum(x.^2)

# Gradient
grad0(x) = 2*to_row(x)

### -----------------------------------------------------------------------
###                             Function 1
### -----------------------------------------------------------------------

function func1(x)
    f = 0;
    for x_i in x
	f += x_i^3;
    end
    return f
end

function grad1(x)
    n = length(x);
    gradient = zeros(1,n);
    for i in 1:n
        gradient[1,i] = 3*x[i]^2
    end
    return gradient
end

### -----------------------------------------------------------------------
###                             Function 2
### -----------------------------------------------------------------------

func2(x) = prod(x)

function grad2(x)
    n = length(x);
    g = zeros(1,n);
    product = func2(x)
    for i in 1:n
        if( abs(x[i]) < 1e-10 )
            g[1,i] = product / x[i];
        else
            backup = x[i];
            x[i] = 1;
            g[1,i] = func2(x);
            x[i] = backup;
        end
    end
    return g
end

### -----------------------------------------------------------------------
###                             Function 3
### -----------------------------------------------------------------------

func3(x) = -sum(log(1 + exp(-x)))

function grad3(x)
    return  to_row( 1. ./ (1 + exp(x)) )
end

### -----------------------------------------------------------------------
###        A function to compute the derivative numerically
### -----------------------------------------------------------------------
function finite_difference(func,x)
    y = func(x);
    n = length(x);
    m = length(y);

    ## @printf "n %d, m %d" n m
    
    delta = 1e-6;
    g = zeros(m,n);
    fx = func(x);
    for i = 1:n
	e_i = zeros(n);
	e_i[i] = 1;
	g[:,i] = (func(x[:] + delta*e_i) - fx)/delta;
    end
    return g
end


### -----------------------------------------------------------------------
###                      Do some naive testing
### -----------------------------------------------------------------------

function test_derivatives(n_test)

    for i = 1:n_test
        
        x = rand(5)
        tol = 1e-4
        
        analytic_gradient = grad1(x)
        finite_difference_gradient =  finite_difference(func1, x)
        assert( norm( analytic_gradient - finite_difference_gradient ) < tol )

        analytic_gradient = grad2(x)
        finite_difference_gradient =  finite_difference(func2, x)
        assert( norm( analytic_gradient - finite_difference_gradient ) < tol )

        analytic_gradient = grad3(x)
        finite_difference_gradient =  finite_difference(func3, x)
        assert( norm( analytic_gradient - finite_difference_gradient ) < tol )

    end
end

test_derivatives(100)
