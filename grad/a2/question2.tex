\newpage
\section{Computation Questions}


\subsection{Proximal-Gradient}


If you run the demo \emph{example\_group.jl}, it will load a dataset and fit a multi-class logistic regression (softmax) classifier. This dataset is actually \emph{linearly-separable}, so there exists a set of weights $W$ that can perfectly classify the training data (though it may be difficult to find a $W$ that perfectly classifiers the validation data). However, 90\% of the columns of $X$ are irrelevant. Because of this issue, when you run the demo you find that the training error is $0$ while the test error is something like $0.2980$.

\begin{enumerate}
\item Write a new function, \emph{logRegSoftmaxL2}, that fits a multi-class logistic regression model with L2-regularization (this only involves modifying the objective function). Hand in the modified loss function and report the validation error achieved with $\lambda = 10$ (which is the  best value among powers to 10). Also report the number of non-zero parameters in the model and the number of original features that the model uses.
  
\begin{lstlisting}%
[frame=single, caption={\emph{logRegSoftmaxL2} modified objective function}]
function funObj(w)
    (f,g) = softmaxObj(w,X,y,k)
    f = f + 0.5*lambda*w[:]'*w[:]
    g = g + lambda * w
    return (f,g)
end
\end{lstlisting}

\ans{
  For $\lambda=10$ the following results are acheived \\
  \fbox{\tt trainError = 0.006; validError = 0.274; nModelParams = 500; nFeaturesUsed = 100}
}

\item While L2-regularization reduces overfitting a bit, it still uses all the variables even though 90\% of them are irrelevant. In situations like this, L1-regularization may be more suitable. Write a new function, \emph{logRegSoftmaxL1}, that fits a multi-class logistic regression model with L1-regularization. You can use the function \emph{findMinL1}, which minimizes the sum of a differentiable function and an L1-regularization term.  Report the number of non-zero parameters in the model and the number of original features that the model uses.

\begin{lstlisting}%
[frame=single, caption={\emph{logRegSoftmaxL1}  function}]
function logRegSoftmaxL1(X,y;lambda=0.1)
  (n,d) = size(X)
  k = maximum(y)
  W = zeros(d,k)

  funObj(w) = softmaxObj(w,X,y,k)
  W[:] = findMinL1(funObj,W[:],lambda,maxIter=500)

  # Make linear prediction function
  predict(Xhat) = mapslices(argmax,Xhat*W,dims=2)

  return LinearModel(predict,W)
end
\end{lstlisting}

\ans{
  For $\lambda=10$ the following results are acheived \\
  \fbox{\tt trainError = 0.048; validError = 0.08; nModelParams = 35; nFeaturesUsed = 19}
}

\item L1-regularization achieves sparsity in the \emph{model parameters}, but in this dataset it's actually the \emph{original features} that are irrelevant. We can encourage sparsity in the original features by using \emph{group} L1-regularization. Write a new function, \emph{proxGradGroupL1}, to allow (disjoint) \emph{group} L1-regularization. Use this within a new function, \emph{softmaxClassiferGL1}, to fit a group L1-regularized multi-class logistic regression model (where \emph{rows} of $W$ are grouped together and we use the L2-norm of the groups).  Hand in both modified  functions (\emph{logRegSoftmaxGL1} and \emph{proxGradGroupL1}) and report the validation error achieved with $\lambda=10$. Also report the number of non-zero parameters in the model and the number of original features that the model uses.

\begin{lstlisting}%
[frame=single, caption={\emph{proxGradGroupL1}  function}]
function proxGradGroupL1(funObj, w, groupXAdj, groupAdj, lambda;maxIter=100,epsilon=1e-2)
  # Minimizes funObj + w * \sum_{g a group} \sqrt{ \sum_{j in g} w_j^2 }
  # w: initial guess
  # lambda: value of L1-regularization parmaeter
  # Note on groupXAdj and groupAdj
  #  The group weight indices are encoded in these two vectors 
  #  groupXAdj has dimension numGroups+1
  #  weights for group i have indices [ groupAdj[groupXAdj[i]], ..., groupAdj[groupXAdj[i+1]] )
  # maxIter: maximum number of iterations
  # epsilon: stop if the gradient gets below this
  #
  # We do a heuristic and simple line search here.

  # Initial step size and sufficient decrease parameter
  groupXAdj = convert(VecInt, groupXAdj[:])
  @assert(groupXAdj[end] == size(w,1)+1)
  @assert(size(groupAdj,1) == size(w,1))
  alpha = 1
  (f,g) = funObj(w)
  nGroups = size(groupXAdj,1)-1

  # A single proximal gradient iteration
  function _singleIteration(w_,f_,g_,alpha_)
    wNew_ = w_ - alpha_*g_
    for i=1:nGroups
      beg_id = groupXAdj[i]
      end_id = groupXAdj[i+1]-1
      wg = wNew_[ groupAdj[beg_id:end_id] ]
      wg_l2 = norm(wg,2)
      wg = (wg / wg_l2) .* max.(0., wg_l2 .- alpha_*lambda) 
      wNew_[ groupAdj[beg_id:end_id] ] = wg
    end    
    (fNew_,gNew_) = funObj(wNew_)
    return (wNew_, fNew_, gNew_)
  end

  # Evaluate the regularizer
  function _regulTerm(w_)
    ans = 0
    for i=1:nGroups
      beg_id = groupXAdj[i]
      end_id = groupXAdj[i+1]-1
      ans = ans + norm(w_[ groupAdj[beg_id:end_id] ],2)
    end
    return ans
  end

  @printf("%6s %15s %15s %15s %6s \n","n", "alpha", "obj", "optCond", "nHalving")
  for i in 1:maxIter
    
    (wNew, fNew, gNew) = _singleIteration(w,f,g,alpha)

    # Decrease the step-size if we increased the function
    nHalving = 0
    while fNew + lambda*_regulTerm(wNew) > f + lambda*_regulTerm(w) 
      alpha /= 2
      (wNew, fNew, gNew) = _singleIteration(w,f,g,alpha)
      nHalving = nHalving + 1
    end

    #If line-search was not needed increase alpha
    if nHalving == 0
       alpha = alpha * 1.3
    end

    # Check if line-search is failing
    if nHalving > 30
      @printf("Failure, reached max line search \n")
      return w
    end
    
    # Print out some diagnostics
    optCond = norm(wNew-w,2)/alpha
    @printf("%6d %15.5e %15.5e %15.5e %6d \n",i,alpha,fNew+lambda*_regulTerm(wNew), optCond, nHalving)

    # Accept the new parameters/function/gradient
    w = wNew
    f = fNew
    g = gNew

    # We want to stop if the gradient is really small
    if optCond < epsilon
      @printf("Problem solved up to optimality tolerance\n")
      return w
    end
  end

  @printf("Reached maximum number of iterations\n")
  return w

end
\end{lstlisting}

\begin{lstlisting}%
[frame=single, caption={\emph{softMaxClassifierGL1}  function}]
# Multi-class softmax with Group  regularization
function softMaxClassifierGL1(X,y;lambda=0.1)
  (n,d) = size(X)
  k = maximum(y)

  # Each column of 'w' will be a logistic regression classifier
  W = zeros(d,k)

  # Create the group indices -- Each row of W should be a group
  # w is vecotrized version of W in  column major format.
  groupXAdj = convert(VecInt, zeros(d+1) .+ 1)
  groupAdj = convert(VecInt, zeros(d*k) .+ 1)
  for i=1:d
    groupXAdj[i+1] = groupXAdj[i]+k
    groupAdj[groupXAdj[i]:(groupXAdj[i+1]-1)] = i .+ d.*VecInt(0:(k-1))
  end

  _funObj(w_) = softmaxObj(w_,X,y,k)
  W[:] = proxGradGroupL1(_funObj,W[:],groupXAdj,groupAdj,lambda,maxIter=500)

  # Make linear prediction function
  predict(Xhat) = mapslices(argmax,Xhat*W,dims=2)

  return LinearModel(predict,W)
end
\end{lstlisting}

\ans{
  For $\lambda=10$ the following results are acheived \\
  \fbox{trainError = 0.022; validError = 0.054; nModelParams = 115; nFeaturesUsed = 23} 
}



\ans{Finally, here are the sparsity patterns for all the three cases with $\lambda=10$.}
\begin{center}
\includegraphics[width=0.25\linewidth]{img/q2-l2.pdf}\hfil
\includegraphics[width=0.25\linewidth]{img/q2-l1.pdf}\hfil
\includegraphics[width=0.25\linewidth]{img/q2-group-l1.pdf} \\
\makebox[0.25\linewidth][c]{L2}\hfil
\makebox[0.25\linewidth][c]{L1}\hfil
\makebox[0.25\linewidth][c]{GL1}
\end{center}

\end{enumerate}


\newpage
\subsection{Coordinate Optimization}

The function \emph{example\_CD.jl} loads a dataset and tries to fit an L2-regularized least squares model using coordinate descent. Unfortunately, if we use $L_f$ as the Lipschitz constant of $\nabla f$, the runtime of this procedure is $O(d^3 + nd^2\frac{L_f}{\mu}\log(1/\epsilon))$. This comes from spending $O(d^3)$ computing $L_f$, having an iteration cost of $O(nd)$, and requiring $O(d\frac{L_f}{\mu}\log(1/\epsilon))$ iterations to reach an accuracy of $\epsilon$. This non-ideal runtime is also reflected in practice: the algorithm's iterations are relatively slow and it often takes over 200 ``passes'' through the data for the parameters to stabilize.

\begin{enumerate}
\item Modify this code so that the runtime of the algorithm is $O(nd\frac{L_c}{\mu}\log(1/\epsilon))$, where $L_c$ is the Lipschitz constant of \emph{all} partial derivatives $\nabla_i f$. You can do this by increasing the step-size to $1/L_c$ (the coordinate-wise Lipschitz constant given by $\max_j\{\norm{x_j}^2\} + \lambda$ where $x_j$ is column $j$ of the matrix $X$), and modifying hte iterations so they have a cost of $O(n)$ instead of $O(nd)$.
  Hand in your code and report an estimate of the change in time and number of iterations.

\ans{Finding $L_c$ using the mentioned method in $O(nd)$ time is trivial. To find $\nabla_if$ in $O(n)$ time, we note that $\nabla_if = X[:,i]^T r + \lambda w_i$, where $r=Xw-y$. Therefore, we can only calculate $r$ once (and optionally recompute it every $d$ iterations to combat numerical errors) and then update it at each iteration via $r \gets r - (1/L)\nabla_if X[:,i] $ at the cost of $O(n)$. The full algorithm will then become as shown in the next code block. The changes in iteration performance and iteration time cost are shown in Table \ref{tab:q2-cd}. }

\begin{lstlisting}%
[frame=single, caption={Changes for \emph{Coordinate Descent} $O(n)$ iteration cost}]
@with_kw struct  leastSquareCDParams
    X::MatFloat
    y::MatFloat
    lambda::Float
    maxPasses::Int
    progTol::Float
    verbose::Bool
end

function leastSquareCDQ1(p::leastSquareCDParams)

    # record states
    timer = a2.Timer();
    stats = Dict("time" => [], "delta" => [], "w" => nothing)

    # I don't want to change the function
    X = p.X
    y = p.y
    lambda = p.lambda
    progTol = p.progTol
    verbose = p.verbose
    maxPasses = p.maxPasses

    d = size(X,2)
    w = zeros(d,1)


    ## Run and time coordinate descent to minimize L2-regularization logistic loss

    # Time precomputations
    timerTick(timer);
    
    # Find norm of cols
    colnormsq = sum(X.*X, dims=1)
    colnorm = sqrt.(colnormsq)

    # Find Lc
    L = maximum(colnormsq) + lambda; 

    # Start running coordinate descent
    w_old = copy(w);

    # Compute r outside, and only update its relevant rows
    r =  X*w - y

    timerTock(timer);
    
    for k in 1:maxPasses*d

        timerTick(timer);
        
        # Choose variable to update 'j'
        j = rand(1:d)

        # Compute partial derivative 'g_j'
        g_j = dot(X[:,j],r) + lambda * w[j]
        
        # Update variable
        w[j] -= (1/L)*g_j;

        # now update r
        r -= (1/L) * g_j * X[:,j]

        # Check for lack of progress after each "pass"
        # - Turn off computing 'f' and printing progress if timing is crucial
        timerTock(timer);

        if mod(k,d) == 0
            timerTick(timer)
            r = X*w - y # it is okay to recompute r once in a long while
            f = (1/2)norm(r)^2 + (lambda/2)sum(w .* w)
            delta = norm(w-w_old,Inf);
            timerTock(timer)
            
            push!(stats["delta"], delta)
            push!(stats["time"], timerElapsed(timer))

            if verbose
                @printf("Passes = %d, function = %.4e, change = %.4g\n",k/d,f,delta);
            end
            
            if delta < progTol
                @printf("Parameters changed by less than progTol on pass\n");
                break;
            end
            
            timerTick(timer)
            w_old = copy(w);
            timerTock(timer)            
        end

    end

    stats["w"] = w[:]
    return stats
end
\end{lstlisting}%

\newpage
\item While it doesn't improve the worst-case time complexity (without making stronger assumptions), you might expect to improve performance by using a more-clever choice of step-size. Modify the code to compute the optimal step-size (which you can do in closed-form without increasing the runtime), and report the effect of using the optimal step-size on the time and number of iterations.

\ans{ It is trivial to verify that the optimum step-size for coordinate $j$ is $\frac{1}{\norm{x_j}^2 + \lambda}.$ Therefore, we only need to make the following change to the algorithm.}

\begin{lstlisting}%
[frame=single, caption={Changes for \emph{Coordinate Descent} with optimal step size (i.e., Gauss-Seidel)}]
# Outside the iteration loop:
colnormsq = sum(X.*X, dims=1)

# The coordinate descent iteration for coord j:
alpha_opt = 1 / ( colnormsq[j] + lambda)
w[j] -= alpha_opt * g_j;
r -= alpha_opt * g_j  * X[:,j]
\end{lstlisting}%


\ans{Here, we summarize the statistics for the results of the improvements implemented for both parts one and two. Figure \ref{fig:q2-cd} shows the norm of progress per number of passes over all coordinates and time. As expected, after implementing part one, iterations become faster, but the rate of convergence stays the same. After using the optimal step size, convergence also becomes faster. The large precomputation time of the naive method is clear in Figure \ref{fig:q2-cd}-a. Table \ref{tab:q2-cd} shows the computation time, the performed number of iteration, and the distance of the obtained solution $w$ from the exact solution $w^*$.}
\begin{figure}[H]
\centering
\includegraphics[width=0.4\linewidth]{img/q2-cd-deltavstime.pdf}\hfil
\includegraphics[width=0.4\linewidth]{img/q2-cd-deltavsiter.pdf} \\
\makebox[0.4\linewidth][c]{(a) Progress per time (seconds)} \hfil
\makebox[0.4\linewidth][c]{(b) Progress per pass} \\
\caption{Progress of each of the CD methods}
\label{fig:q2-cd}
\end{figure}

\begin{table}[H]
\centering
\caption{Statisctics for each of the CD methods}
\label{tab:q2-cd}
\begin{tabular}{cccc}
  \hline
  method &\# of iterations &total time  &$\|w-w^*\|_2/\|w^*\|$ \\
  \hline
  naive         &$1000$   &$20.3$(s)  &$0.79$ \\
  after part 1  &$1000$   &$0.60$(s)  &$0.79$ \\
  after part 2  &$45$    &$0.02$(s)   &$10^{-6}$ \\
  \hline
\end{tabular}
\end{table}

\end{enumerate}

\newpage
\subsection{Stochastic Gradient}

If you run the demo \emph{example\_SG.jl}, it will load a dataset and try to fit an L2-regularized logistic regression model using 10 ``passes'' of stochastic gradient using the step-size of $\alpha_t = 1/\lambda t$ that is suggested in many theory papers. Note that in other high-level languages (like R/Matlab/Python) this demo would run really slowly so you would need to write the inner loop in a low-level language like C, but in Julia you can directly write the stochastic gradient code and have it run fast.

Unfortunately, despite Julia making this code run fast compared to other high-level languages, the performance of this stochastic gradient method is atrocious. It often goes to areas of the parameter space where the objective function overflows and the final value is usually in the range of something like $6.5-7.5 \times 10^4$. This is quite far from the solution of $2.7068 \times 10^4$ and is even worse than just choosing $w=0$ which gives $3.5 \times 10^4$. (This is unlike gradient descent and coordinate optimization, which never increase the objective function if your step-size is small enough.)

\begin{enumerate}
\item Although $\alpha_t = 1/\lambda t$ gives the best possible convergence rate in the worst case, in practice it's typically horrible (as we're not usually opitmizing the hardest possible $\lambda$-strongly convex function). Experiment with different choices of step-size sequence to see if you can get better performance. Report the step-size sequence that you found gave the best performance, and the objective function value obtained by this strategy for one run.

\ans{I have tried the following strategies for the stepsize:
(a) $\frac{n}{k \lambda}$, 
(b)  $\frac{10}{k \lambda}$,
(c)  $\frac{1}{k \lambda}$,
(d)  $\frac{0.1}{k \lambda}$
(e)  $\frac{10}{\sqrt{k} \lambda}$,
(f)  $\frac{1}{\sqrt{k} \lambda}$,
(g)  $\frac{0.1}{\sqrt{k} \lambda}$,
(h)  $10^{-2}$,
(i)  $10^{-3}$,
(j)  $10^{-4}$,
(k)  $10^{-5}$.
The results are shown in Figure \ref{fig:q3-sg-stepsize} for 100 passes. The best objective value is obtained using case (g) (i.e, $\alpha(k) = \frac{0.1}{\sqrt{k} \lambda}$ which gives the final objective $f=2.7096 \times 10^{4}$}.

\begin{figure}[H]
  \centering
\includegraphics[width=0.4\linewidth]{img/q2-sg-p1-delta.pdf}\hfil
\includegraphics[width=0.4\linewidth]{img/q2-sg-p1-obj.pdf} \\
\makebox[0.4\linewidth][c]{(a) Progress} \hfil
\makebox[0.4\linewidth][c]{(b) Objective} \\
\caption{Experimenting with SG stepsize}
\label{fig:q3-sg-stepsize}
\end{figure}
  
\newpage
\item Besides tuning the step-size, another strategy that often improves the performance is using a (possibly-weighted) average of the iterations $w^t$. Explore whether this strategy can improve performance.  Report the performance with an averaging strategy, and the objective function value obtained by this strategy for one run. Note that the best step-size sequence with averaging might be different than without averaging (usually you can use bigger steps when you average).

\ans{Every $n$ iterations, we set $w$ to be the average of the last $n$ solution values. We can always pick the average of iterations by keeping the track of the sum of the iterates.}
\begin{lstlisting}%
[frame=single, caption={Stochastic Gradient with Solution Averaging}]
# Outside the iteration loop:
sum_w = zeros(d,1)

# Inside the iteration loop:

# Take thes stochastic gradient step
w -= alpha*g_i;
# Update the total sum
sum_w += w
# averaging
if mod(k,n) == 0
 w = sum_w /n 
 sum_w = zeros(d,1)
end 
\end{lstlisting}%
\ans{ The best result is obtained with case (i) (i.e., $\alpha(k) = 10^{-3}$, with final objective $f=2.7093 \times 10^{4}$. Figure \ref{fig:q3-sg-aveunk} shows the progress and objective history. We can see that the results are smoother compared to Figure \ref{fig:q3-sg-stepsize}, where no averaging was used.}
\begin{figure}[H]
  \centering
\includegraphics[width=0.4\linewidth]{img/q2-sg-p2-delta.pdf}\hfil
\includegraphics[width=0.4\linewidth]{img/q2-sg-p2-obj.pdf} \\
\makebox[0.4\linewidth][c]{(a) Progress} \hfil
\makebox[0.4\linewidth][c]{(b) Objective} \\
\caption{Experimenting with SG stepsize}
\label{fig:q3-sg-aveunk}
\end{figure}
  
\newpage
\item A popular variation on stochastic is AdaGrad, which uses the iteration
\[
w^{k+1} = w^k - \alpha_k D_k \nabla f(w^k),
\]
where the element in position $(j,j)$ of the diagonal matrix $D_k$ is given by $1/\sqrt{\delta + \sum_{k'=0}^k(\nabla_j f_{i_{k'}}(w^{k'}))^2}$. Here, $i_k$ is the example $i$ selected on iteration $k$ and $\nabla_j$ denotes element $j$ of the gradient (and in AdaGrad we typically don't average the steps).  Implement this algorithm and experiment with the tuning parameters $\alpha_t$ and $\delta$. Hand in your code as well as the best step-size sequence you found and again report the performance for one run.

\ans{The changes required in the code are shown below.}
\begin{lstlisting}%
[frame=single, caption={Changes required for AdaGrad}]
# Outside the iteration loop:
# Helper for finding the diagonal of D
DHelper = zeros(size(w,1),size(w,2));

# Inside the iteration loop:

# Update D Helper
DHelper += g_i .* g_i
# Find diagonal of D
D = 1. ./ sqrt. ( delta .+ DHelper )
# Take the stochastic gradient step
w -= alpha * D .* g_i;
\end{lstlisting}%
\ans{The convergence rate does not seem to improve much. We tried parameters $\delta \in \{1.$, $10^{-1}$, $10^{-2}$, $10^{-3}$, $10^{-4}$, $10^{-5}$, $10^{-6}\}$ and $\alpha_k \in \{\frac{10}{\sqrt{k} \lambda},\frac{1}{\sqrt{k} \lambda},\frac{0.1}{\sqrt{k} \lambda},10^{-2},10^{-3},10^{-4}\}$. The best result is acheived using the parameters $\delta=1.$ and $\alpha_k=10^{-2}$  and gives the final objective $f=2.7157\times 10^{4}$}.

\newpage
\item Implement the SAG algorithm with a step-size of $1/L$, where $L$ is the maximum Lipschitz constant across the training examples ($L = \frac{1}{4}\max_i\{\norm{x^i}^2\} + \lambda$).  \ans{Hand in your code and again report the performance for one run}.

\begin{lstlisting}%
[frame=single, caption={Changes required for SAG}]
# Outside the iteration loop:

# Find step size
sumX2 = sum(X.*X,dims=2) # sum over features
L = 0.25*maximum(sumX2) + lambda

# Store all prev gradients and their sum
prev_grads = zeros(n, d)
sum_grads = zeros(d, 1)

# Inside the iteration loop:

# Update prev gradients and their sum
sum_grads = sum_grads - prev_grads[i, :] + g_i 
prev_grads[i, :] = g_i

# Update the solution
w -= alpha * sum_grads/n
\end{lstlisting}%
\ans{The performance is shown in the following figures. The final objective is $f=2.7068\times10^{4}$, which is indeed the optimum result. Unlike the previous method, we can see from Figure \ref{fig:q3-sag}-a that the progress maintains its exponential rate, unlike the previous cases.}
\begin{figure}[H]
  \centering
\includegraphics[width=0.4\linewidth]{img/q2-sg-p4-delta.pdf}\hfil
\includegraphics[width=0.4\linewidth]{img/q2-sg-p4-obj.pdf} \\
\makebox[0.4\linewidth][c]{(a) Progress} \hfil
\makebox[0.4\linewidth][c]{(b) Objective} \\
\caption{Results for the SAG method}
\label{fig:q3-sag}
\end{figure}

  
\end{enumerate}


%% Emacs latex configurations; please keep at the end of the file.
%% Local Variables:
%% TeX-master: "root"
%% End: 
