# Load X and y variable
using JLD
data = load("logisticData.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

# Standardize columns and add bias
n = size(X,1)
include("misc.jl")
(X,mu,sigma) = standardizeCols(X)
X = [ones(n,1) X]

# Standardize columns of test data, using mean/std from train data
t = size(Xtest,1)
Xtest = standardizeCols(Xtest,mu=mu,sigma=sigma)
Xtest = [ones(t,1) Xtest]



##################################################
##### Using different models #####
##################################################
# using LeastSquares
#include("leastSquares.jl")
#model = binaryLeastSquares(X,y)

#################################################
# using LogisticRegression with no regularization
#include("logReg.jl")
#model = logReg(X,y)

#################################################
# using LogisticRegression with L2 regularization
#include("logReg.jl")
#lambda = 1.0;
#model = logRegL2(X,y, lambda)

#################################################
# using LogisticRegression with L1 regularization
#include("logReg.jl")
#lambda = 1.0;
#model = logRegL1(X,y, lambda)

#################################################
# using LogisticRegression with L0 regularization
include("logReg.jl")
lambda = 1.0;
model = logRegL0(X,y, lambda)
##################################################


# Count number of non-zeroes in model
numberOfNonZero = sum(model.w .!= 0)
@show(numberOfNonZero)

# Compute training and validation error
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@show(trainError)
yhat = model.predict(Xtest)
validError = mean(yhat .!= ytest)
@show(validError)