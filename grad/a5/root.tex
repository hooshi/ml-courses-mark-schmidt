\documentclass{article}

\usepackage{fullpage}
\usepackage{color}
\usepackage{amsmath}
\usepackage{url}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{amssymb}
\usepackage{nicefrac}
\usepackage{listings} % For displaying code
\usepackage{algorithm2e} % pseudo-code

% Answers
\def\ans#1{\par\gre{Answer: #1}}
%\def\ans#1{} % Comment this line to produce document with answers

% Colors
\definecolor{blu}{rgb}{0,0,1}
\def\blu#1{{\color{blu}#1}}
\definecolor{gre}{rgb}{0,.5,0}
\def\gre#1{{\color{gre}#1}}
\definecolor{red}{rgb}{1,0,0}
\def\red#1{{\color{red}#1}}
\def\norm#1{\|#1\|}

% Math
\def\R{\mathbb{R}}
\def\argmax{\mathop{\rm arg\,max}}
\newcommand{\argmin}[1]{\mathop{\hbox{argmin}}_{#1}}
\newcommand{\mat}[1]{\begin{bmatrix}#1\end{bmatrix}}
\newcommand{\alignStar}[1]{\begin{align*}#1\end{align*}}
\def\half{\frac 1 2}
\def\cond{\; | \;}

% LaTeX
\newcommand{\fig}[2]{\includegraphics[width=#1\textwidth]{img/#2}}
\newcommand{\centerfig}[2]{\begin{center}\includegraphics[width=#1\textwidth]{img/#2}\end{center}}
\def\items#1{\begin{itemize}#1\end{itemize}}
\def\enum#1{\begin{enumerate}#1\end{enumerate}}


\begin{document}

\title{CPSC 540 Assignment 5 (due April 5 at midnight)}
\author{}
\date{}
\maketitle
\vspace{-4em}

The assignment instructions are the same as for the previous assignment.


\blu{\enum{
\item Name(s): Zicong Fan, Shayan Hoshyari, Dylan Green 
\item Student ID(s): 11205168, 81382153, 22420153
\item Ugrad ID(s): u7c1b, u3t0b, n1p0b
}}


\section{Undirected Graphical Models}

\subsection{Conditional UGM}

Consider modeling the dependencies between sets of binary variables $x_j$ and $y_j$ with the following UGM which is a variation on a stacked RBM:
%\centerfig{.6}{weirdRBM}
Computing univariate marginals in this model will be NP-hard in general, but the graph structure allows efficient block updates by conditioning on suitable subsets of the variables (this could be useful for designing approximate inference methods).
For each of the conditioning scenarios below, \blu{draw the conditional UGM and informally comment on how expensive it would be to compute univariate marginals} (for all variables) in the conditional UGM.
\enum{
\item Conditioning on all the $x$ and $h$ values.
\ans{Conditional UGM:
\centerfig{.3}{1_1_1}
Computing univariate marginals here is trivial as they are simply the conditional probabilities given the $x$ and $h$ values.}
\item Conditioning on all the $z$ and $y$ values.
\ans{Conditional UGM:
\centerfig{.3}{1_1_2}
Computing marginals for the $x$ values is trivial for the reason stated above. Marginals for the $h$ values can be computed in $O(dk^2)=O(k^2)$ time using the forward-backward algorithm (here $d$ is constant). }\newpage
\item Conditioning on all the $x$ and $z$ values.
\ans{Conditional UGM:
\centerfig{.3}{1_1_3}
The graph shown has treewidth 2, so marginals can be computed in $O(dk^3) = O(k^3)$ time with message passing.}}




\subsection{Fitting a UGM to PINs}


The function \emph{example\_UGM.jl} loads a dataset $X$ containing samples of PIN numbers, based on the probabilities from the article at this URL: \url{http://www.datagenetics.com/blog/september32012}.\footnote{I got the probabilities from reverse-engineered heatmap here: \url{http://jemore.free.fr/wordpress/?p=73}.}

This function fits a UGM model to the dataset, where all node/edge parameters are untied and the graph is empty. It then performs decoding/inference/sampling in the fitted model. The decoding is reasonable (it's $x= \mat{1& 2 & 3 & 4}$) and the univariate marginals are reasonable (it says the first number is $1$ approximately 40\% of the time and the last number is 4 approximately 20\% of the time), but because it assumes the variables are independent we can see that this is not a very good model:
\enum{
\item The sampler doesn't tend to generate the decoding ($x= \mat{1& 2 & 3 & 4}$) as often as we would expect. Since it happens in more than 1/10 of the training examples, we should be seeing it in more than 1/10 of the samples.
\item Conditioned on the first three numbers being $1 \; 2 \; 3$, the probability that the last number is 4 is only around 20\%, whereas in the data it's more than 90\% in this scenario.
}
In this question, you'll explore using (non-degenerate UGMs) to try to fix the above issues:
\enum{
\item \blu{Write an equation for $p(x_1, x_2, x_3, x_4)$ in terms of the parameters $w$ being used by the code.}
\ans{\\Since there are no edges in this model, we have that $$p(x_1, x_2, x_3, x_4) = \frac{1}{Z}\tilde{p}(x) = \frac{1}{Z}\exp\left(\sum_{j=1}^4w^j_{x_j}\right)$$, where $$Z = \sum_{x'_1=1}^{10}\sum_{x'_2=1}^{10}\sum_{x'_3=1}^{10}\sum_{x'_4=1}^{10}\exp\left(\sum_{j=1}^4w^j_{x'_j}\right)$$}
\item \blu{How would the answer to the previous question change (in terms of $w$ and $v$) if we use \texttt{E = [1 2]}?}
\ans{We will now have $$p(x_1, x_2, x_3, x_4) = \frac{1}{Z}\tilde{p}(x) = \frac{1}{Z}\exp\left(\sum_{j=1}^4w^j_{x_j} + v^{1-2}_{x_1, x_2}\right)$$, where $v^{1-2}_{x_1, x_2}$ is the $v$ value for nodes 1,2 taking values $x_1, x_2$ respectively and $$Z = \sum_{x'_1=1}^{10}\sum_{x'_2=1}^{10}\sum_{x'_3=1}^{10}\sum_{x'_4=1}^{10}\exp\left(\sum_{j=1}^4w^j_{x'_j} + v^{1-2}_{x'_1, x'_2}\right)$$}
\item Modify the demo to use a chain-structured dependency. \blu{Comment on whether this fixes each of the above 2 issues.}
\ans{The code is modified by setting \texttt{E = [1 2; 2 3; 3 4]}. The above issues are not fixed, but the model does improve somewhat: the decoding is generated by the sampler roughly 4\% of the time (as opposed to 0.3\% in the disconnected case) and the conditional probability of getting 4 as the last number given that the first three numbers are 1 2 3 increases to roughly 50\%.}
\item Modify the demo to use a completely-connected graph. \blu{Comment on whether this fixes each of the above 2 issues.}
\ans{The code is modified by setting \texttt{E = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4]}. Again this does not fix the issues, but gives an incremental improvement over the chain structured model: the decoding is generated roughly 75\% of the time, and the conditional probability of getting 4 as the last number given that the first three numbers are 1 2 3 increases to roughly 75\%.}
\item \blu{What would the effect of higher-order potentials be? What would a disdavantage of higher-order potentials be?}
\ans{Higher-order potentials would give the model more flexibility to account for higher-order relationships in the data. For example, we would likely get closer to correctly modelling the density given parameters like $w_{2,3,4,2,3,4}$. This would come at the cost of increased complexity during training, since a set of $m$-th order coordinates introduces $k^m$ new parameters we need to optimize (and optimization requires inference). }
}
If you want to further explore UGMs, there are quite a few Matlab demos on the UGM webpage:\\
\url{https://www.cs.ubc.ca/~schmidtm/Software/UGM.html}\\
that you can go through which cover all sorts of things like approximate inference and CRFs.


\section{Bayesian Inference}

\subsection{Conjugate Priors}

Consider a $y \in \{1,2,3\}$ following a multinoulli distribution with parameters $\theta = \{\theta_1,\theta_2,\theta_3\}$,
\[
y \cond  \theta \sim \text{Mult}(\theta_1,\theta_2,\theta_3).
\]
We'll assume that $\theta$ follows a Dirichlet distribution (the conjugate prior to the multinoulli) with parameters $\alpha = \{\alpha_1,\alpha_2,\alpha_3\}$,
\[
 \theta \sim \mathcal{D}(\alpha_1,\alpha_2,\alpha_3).
\]
Thus we have
\[
p(y\cond \theta,\alpha) = p(y\cond \theta) = \theta_1^{I(y=1)}\theta_2^{I(y=2)}\theta_3^{I(y=3)}, \quad p(\theta\cond \alpha) = \frac{\Gamma(\alpha_1+\alpha_2+\alpha_3)}{\Gamma(\alpha_1)\Gamma(\alpha_2)\Gamma(\alpha_3)}\theta_1^{\alpha_1-1}\theta_2^{\alpha_2-1}\theta_3^{\alpha_3-1}.
\]
Compute the following quantites:
\enum{
\item \blu{The posterior distribution,
\[
p(\theta\cond y,\alpha).
\]
\ans{
\begin{align*}
p(\theta|y,\alpha) &\propto p(y|\theta, \alpha)p(\theta|\alpha)\\ &\propto \theta_1^{I(y=1)}\theta_2^{I(y=2)}\theta_3^{I(y=3)}\theta_1^{\alpha_1-1}\theta_2^{\alpha_2-1}\theta_3^{\alpha_3-1}\\&=\theta_1^{\beta_1-1}\theta_2^{\beta_2-1}\theta_3^{\beta_3-1}
\end{align*}Thus by proportionality we have $\theta|y,\alpha \sim \mathcal{D}(\beta_1, \beta_2, \beta_3)$, or
\begin{align*}
p(\theta|y,\alpha) &= \frac{\Gamma(\alpha_1 + \alpha_2 + \alpha_3 + 1)}{\Gamma(\alpha_1 + I(y=1))\Gamma(\alpha_2 + I(y=2))\Gamma(\alpha_3 + I(y=3))}\theta_1^{\beta_1-1}\theta_2^{\beta_2-1}\theta_3^{\beta_3-1}\\&=\frac{\alpha_1 + \alpha_2 + \alpha_3}{\alpha_1^{I(y=1)}\alpha_2^{I(y=2)}\alpha_3^{I(y=3)}}\frac{1}{D(\alpha)}\theta_1^{(\alpha_1 - 1 + I(y=1))}\theta_2^{(\alpha_2 - 1 + I(y=2))}\theta_3^{(\alpha_3 - 1 + I(y=3))}
\end{align*}}
\item \blu{The marginal likelihood of $y$ given the hyper-parameters $\alpha$,
\[
p(y\cond \alpha) = \int p(y,\theta\cond \alpha)d\theta,
\]}
\ans{
\begin{align*}
p(y\cond \alpha) &= \int p(y,\theta\cond \alpha)d\theta = \int p(y|\theta,\alpha)p\theta|\alpha)d\theta\\&= \int \theta_1^{I(y=1)}\theta_2^{I(y=2)}\theta_3^{I(y=3)}\frac{1}{D(\alpha)}\theta_1^{\alpha_1-1}\theta_2^{\alpha_2-1}\theta_3^{\alpha_3-1}\\&=\frac{1}{D(\alpha)}\int \theta_1^{\beta_1-1}\theta_2^{\beta_2-1}\theta_3^{\beta_3-1} d\theta\\&= \frac{D(\alpha^+)}{D(\alpha)}\\&= \frac{\alpha_1^{I(y=1)}\alpha_2^{I(y=2)}\alpha_3^{I(y=3)}}{\alpha_1+\alpha_2+\alpha_3}
\end{align*}}
\item \blu{The posterior mean estimate for $\theta$,
\[
\mathbb{E}_{\theta\cond y,\alpha}[\theta_i] = \int \theta_i p(\theta\cond y,\alpha)d\theta,
\]}
which (after some manipulation) should not involve any $\Gamma$ functions.
}
\ans{Let $\alpha^* = \left\lbrace \beta_j + I(i=j)\right\rbrace_{j=1}^3$. Then
\begin{align*}
\mathbb{E}_{\theta\cond y,\alpha}[\theta_i] &= \int \theta_i p(\theta\cond y,\alpha)d\theta\\&=\frac{1}{D(\alpha^+)}\int \theta_i\theta_1^{\beta_1-1}\theta_2^{\beta_2-1}\theta_3^{\beta_3-1}d\theta\\&=\frac{1}{D(\alpha^+)}\int \theta_1^{(\beta_1-1+I(i=1))}\theta_2^{(\beta_2-1+I(i=2))}\theta_3^{(\beta_3-1+I(i=3))}d\theta\\&=\frac{D(\alpha^*)}{D(\alpha^+)}\\&=\frac{\Gamma(\beta_1+\beta_2+\beta_3)}{\Gamma(\beta_1)\Gamma(\beta_2)\Gamma(\beta_3)}\frac{\Gamma(\beta_1+I(i=1))\Gamma(\beta_2+I(i=2))\Gamma(\beta_3+I(i=3))}{\Gamma(\beta_1+\beta_2+\beta_3+1)}\\&=\frac{\beta_i}{\beta_1+\beta_2+\beta_3} = \frac{\alpha_i+I(y=i)}{\alpha_1+\alpha_2+\alpha_3+1}
\end{align*}}
\item \blu{The posterior predictive distribution for a new independent observation $\tilde{y}$ given $y$,
\[
p(\tilde{y}\cond y,\alpha) =  \int p(\tilde{y},\theta\cond y,\alpha)d\theta.
\]
}
\ans{For $\tilde{y} = i$ we have 
\begin{align*}
p(\tilde{y}=i|y,\alpha) &= \int p(\tilde{y} = i, \theta |y,\alpha) d\theta\\&= \int \theta_i p(\theta\cond y,\alpha)d\theta \\&= \mathbb{E}_{\theta\cond y,\alpha}[\theta_i] \\&= \frac{\beta_i}{\beta_1+\beta_2+\beta_3},
\end{align*} so in the general case we have 
\begin{align*}
p(\tilde{y}|y,\alpha) &= \frac{\beta_1^{I(\tilde{y}=1)}\beta_2^{I(\tilde{y}=2)}\beta_3^{I(\tilde{y}=3)}}{\beta_1+\beta_2+\beta_3}\\&= \frac{(\alpha_1 + I(y=1))^{I(\tilde{y}=1)}(\alpha_2 + I(y=2))^{I(\tilde{y}=2)}(\alpha_3 + I(y=3))^{I(\tilde{y}=3)}}{\beta_1+\beta_2+\beta_3}
\end{align*}
}
}
 Hint: You can use $D(\alpha) =  \frac{\Gamma(\alpha_1)\Gamma(\alpha_2)\Gamma(\alpha_3)}{\Gamma(\alpha_1+\alpha_2+\alpha_3)}$ to represent the normalizing constant of the prior and $D(\alpha^+)$ to give the normalizing constant of the posterior. You will also need to use that $\Gamma(\alpha+1) = \alpha\Gamma(\alpha)$. For some calculations you may find it a bit cleaner to parameterize the posterior in terms of $\beta_j = I(y=j) + \alpha_j$, and convert back once you have the final result.



\section{Very-Short Answer Questions}

Give a short and concise 1-sentence answer to the below questions.

\enum{
\item  Why is it appealing to use tree-structured blocks in block-ICM?
\ans{Tree structured blocks allow for efficient updates; since the conditional UGM is tree structured we can do exact inference in polynomial time using belief propagation. }
\item Why don't we need to compute $Z$ when we use pseudo-likelihood?
\ans{Psuedo-likelihood splits the graph into one variable conditional UGMs and maximizes $p(x_j|x_{\text{nei(j)}})$ for all $i$; the conditionals are computed as $\frac{p(x_j, x_{\text{nei(j)}}) }{p(x_{\text{nei(j)}})}$ and so the $Z's$ in the top and bottom cancel out.}
\item Since deep belief networks are just a big DAG, why is it hard to train them?
\ans{Because in training we are conditioning on the leaves of the DAG, meaning that there is combinatorial "explaining away" (all $z$ values are dependent on one another given the $x$ values).}
\item What is the advantage of using a CRF, modeling $p(y \cond x)$, rather than treating supervised learning as special case of density estimation (modeling $p(y, x)$).
\ans{You can use complicated features $x$ without making inference hard (or at least it will be much easier than if you were to model $x$).}
\item What is an advantage of using residual connections within an RNN?
\ans{Avoids vanishing gradients that result from non-linear transforms in network layers/LSTM blocks.}
\item What is a setting where we would want to use dilated convolutions?
\ans{One setting would be when working with very high resolution images: we want to consider larger regions of the image in the convolution to give more context (since nearby pixels will likely be very similar in a high-res image) but don't want to introduce extra parameters by using a larger convolution.}
\item What is the difference between the posterior and posterior predictive distributions?
\ans{The posterior gives the probability of a set of model parameters given the "training" data, the posterior predictive gives the probability of a particular label for new data given the old ("training") data.}
\item What is the relationship between Bayes factors and empirical Bayes?
\ans{Empirical Bayes is a method for optimizing the hyperparameters of a particular model by maximizing the evidence; Bayes factors are used to compare the maximized evidence of two models to determine which model performs better.}
\item What is a hyper-hyper-parameter?
\ans{In Bayesian learning we can treat the prior's hyperparameters as random variables with a hyper-prior. A hyper-hyperparameter is a parameter used in the hyper-prior.}
\item Why do topic models use the Dirichlet prior?
\ans{The distribution topic proportions $\pi$ is inherently categorical, and the conjugate prior for the categorical distribution is the Dirichlet distribution (and conjugate priors make inference easier).}
\item In what setting is it unnecessary to include the $q$ function in the Metropolis-Hastings acceptance probability?
\ans{In scenarios where transition probabilities are symmetric (e.g. if $p(\hat{x} = i | x = j) =\\ p(x=j|\hat{x} = i)$ for all $i,j$).}
}



\section{Literature Survey}
\ans{Literature survey is included in a separate file.}

 
\end{document}