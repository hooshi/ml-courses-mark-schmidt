#!/usr/bin/env julia

using PyCall
@pyimport matplotlib.pyplot as plt

include("misc.jl");
include("vectorQuantization.jl");


# Read the file names from command line
name_in = readParam(ARGS, "-i", String, "dog.png");
name_out = readParam(ARGS, "-o", String, "quantized_dog.png");
bb = readParam(ARGS, "-b", Int, 1);
should_gray_scale = isParamPresent(ARGS, "-g");
should_show = isParamPresent(ARGS, "-s");
@printf("name in is %s \n", name_in);
@printf("name out is %s \n", name_out);
@printf("grayscale %s \n", should_gray_scale);
@printf("should show %s \n", should_show);
@printf("b is %s \n", bb);

# Open the image
img = plt.imread(name_in)

## Should we gray-scale the image
cmap = nothing
if(should_gray_scale && (size(img,3) >= 3) )
    img = 0.21*img[:,:,3] + 0.72*img[:,:,3] + 0.07*img[:,:,3]
    cmap = "gray"
end

## Cluster the image
(labels, clusters) = quantizeImage(img, bb)
img2 = deQuantizeImage(labels, clusters)


## Save the output
fig=plt.figure()
plt.imshow(img2, cmap=cmap)
plt.axis("off")
plt.savefig(name_out, bbox_inches="tight")

## show if required
if(should_show)
    plt.show()
end
