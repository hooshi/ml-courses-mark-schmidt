function leastSquares(X,y;lambda=0)

	n = size(X,1)
	Z = [ones(n,1) X]

	# Find regression weights minimizing squared error
    if lambda > 0
	    w = (Z'*Z + lambda*Eye(d) )\(Z'*y)
    else 
	    w = (Z'*Z)\(Z'*y)
    end

	# Make linear prediction function
	predict(Xtilde) = [ones(size(Xtilde,1),1) Xtilde]*w

	# Return model
	return LinearModel(predict,w)
end

