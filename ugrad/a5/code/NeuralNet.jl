module nnet

# We use nHidden as a vector, containing the number of hidden units in each layer

## ===========================================================================
##                   Data Scaling
## ===========================================================================
struct Scaler
    mean_vector
    std_vector
end

function create_scaler(X)
    mean_vector = mean(X,1)
    std_vector = std(X, 1, corrected=false, mean=mean_vector)
    return Scaler(mean_vector, std_vector)
end

function apply_scaler(X, scaler)
    Xscaled = zeros(X)
    for j in 1:size(X,2)
        Xscaled[:,j] = (X[:,j] - scaler.mean_vector[j]) / scaler.std_vector[j]
    end
    return Xscaled
end

function unapply_scaler(X, scaler)
    Xscaled = zeros(X)
    for j in 1:size(X,2)
        Xscaled[:,j] = X[:,j] * scaler.std_vector[j] + scaler.mean_vector[j]
    end
    return Xscaled
end

## ==============================================================================
##                    Neural Nets
## ==============================================================================
## TANH
# neural_net_h(z) = tanh.(z)
# neural_net_dh(z) = (sech.(z)).^2

## TANH
function neural_net_h(z)
    answer =  tanh.(z)
    answer[end] = 1
    return answer
end

function neural_net_dh(z)
    answer = (sech.(z)).^2
    answer[end] = 0
    return answer
end

## SIGMOID
# function neural_net_h(z)
#     return 1.0 ./ (1.0 .+ exp(-z))
# end

# function neural_net_dh(z)
#     sigmoid = neural_net_h(z)
#     return sigmoid .* (1 .- sigmoid)
# end

## RELU
# function neural_net_h(z)
#     h = zeros(z)
#     h[z .> 0] = z[z .> 0]
#     return h
# end

# function neural_net_dh(z)
#     h = zeros(z)
#     h[z .> 0] = 1    
# end

# Function that returns total number of parameters
function nParams(d,nHidden)
    
    # Connections from inputs to first hidden layer	
    nParams = d*nHidden[1]

    # Connections between hidden layers
    for h in 2:length(nHidden)
	nParams += nHidden[h-1]*nHidden[h]
    end

    # Connections from last hidden layer to output
    nParams += nHidden[end]

end

# Compute squared error and gradient 
# for a single training example (x,y)
# (x is assumed to be a column-vector)
function backprop(bigW,x,y,nHidden)
    d = length(x)
    nLayers = length(nHidden)

    #### Reshape 'bigW' into vectors/matrices
    W1 = reshape(bigW[1:nHidden[1]*d],nHidden[1],d)
    ind = nHidden[1]*d
    Wm = Array{Any}(nLayers-1)
    for layer in 2:nLayers 
	Wm[layer-1] = reshape(bigW[ind+1:ind+nHidden[layer]*nHidden[layer-1]],nHidden[layer],nHidden[layer-1])
	ind += nHidden[layer]*nHidden[layer-1]
    end
    w = bigW[ind+1:end]

    #### Define activation function and its derivative
    h(z) = neural_net_h(z)
    dh(z) = neural_net_dh(z)
    
    #### Forward propagation
    z = Array{Any}(nLayers)
    z[1] = W1*x
    for layer in 2:nLayers
	z[layer] = Wm[layer-1]*h(z[layer-1])
    end
    yhat = w'*h(z[end])

    r = yhat-y
    f = (1/2)r^2

    #### Backpropagation
    dr = r
    err = dr

    # Output weights
    Gout = err*h(z[end])

    Gm = Array{Any}(nLayers-1)
    if nLayers > 1
	# Last Layer of Hidden Weights
	backprop = err*(dh(z[end]).*w)
	Gm[end] = backprop*h(z[end-1])'

	# Other Hidden Layers
	for layer in nLayers-2:-1:1
	    backprop = (Wm[layer+1]'*backprop).*dh(z[layer+1])
	    Gm[layer] = backprop*h(z[layer])'
	end

	# Input Weights
	backprop = (Wm[1]'*backprop).*dh(z[1])
	G1 = backprop*x'
    else
	# Input weights
	G1 = err*(dh(z[1]).*w)*x'
    end

    #### Put gradients into vector
    g = zeros(size(bigW))
    g[1:nHidden[1]*d] = G1
    ind = nHidden[1]*d
    for layer in 2:nLayers
	g[ind+1:ind+nHidden[layer]*nHidden[layer-1]] = Gm[layer-1]
	ind += nHidden[layer]*nHidden[layer-1]
    end
    g[ind+1:end] = Gout

    return (f,g)
end

# Computes predictions for a set of examples X
function predict(bigW,Xhat,nHidden)
    (t,d) = size(Xhat)
    nLayers = length(nHidden)

    #### Reshape 'bigW' into vectors/matrices
    W1 = reshape(bigW[1:nHidden[1]*d],nHidden[1],d)
    ind = nHidden[1]*d
    Wm = Array{Any}(nLayers-1)
    for layer in 2:nLayers
	Wm[layer-1] = reshape(bigW[ind+1:ind+nHidden[layer]*nHidden[layer-1]],nHidden[layer],nHidden[layer-1])
	ind += nHidden[layer]*nHidden[layer-1]
    end
    w = bigW[ind+1:end]

    #### Define activation function and its derivative
    h(z) = neural_net_h(z)
    dh(z) = neural_net_dh(z)

    #### Forward propagation on each example to make predictions
    yhat = zeros(t,1)
    for i in 1:t
	# Forward propagation
	z = Array{Any}(nLayers)
	z[1] = W1*Xhat[i,:]
	for layer in 2:nLayers
	    z[layer] = Wm[layer-1]*h(z[layer-1])
	end
	yhat[i] = w'*h(z[end])
    end
    return yhat
end

end; # module nnt


## ============================================================================
##                  Example case
## ============================================================================
module example_nnet
import ..nnet

using JLD
using PyPlot

mutable struct ExampleData
    X
    y
    Xtest
    ytest

    scaler_X
    scaler_y

    nHidden
    w
    list_error
    list_iter

    maxIter
    stepSize
    beta
    indexIter

    ExampleData() = new()
end

gData = ExampleData()

function init()
    global gData

    ## Set the data
    data = load("basisData.jld")
    (gData.X,gData.y) = (data["X"],data["y"])
    (gData.Xtest,gData.ytest) = (data["Xtest"],data["ytest"])
    gData.scaler_X = nnet.create_scaler(gData.X)
    gData.scaler_y = nnet.create_scaler(gData.y)

    gData.nHidden = [200 100 10]
    d = 2
    nParams = nnet.nParams(d,gData.nHidden)
    gData.w = randn(nParams,1)
    gData.list_iter = []
    gData.list_error = []

    gData.maxIter = 10000
    gData.beta= 0.9
    gData.stepSize=1e-2
    gData.indexIter = 0
end

function show_params()
    global gData
    
    @show gData.stepSize
    @show gData.beta
    @show gData.maxIter
    @show gData.indexIter
    if( length(gData.list_error) > 0)
        @show gData.list_error[end]
    end
end

function set_stepSize(stepSize)
    global gData
    gData.stepSize = stepSize
end

function set_beta(beta)
    global gData
    gData.beta = beta
end


function set_maxIter(maxIter)
    global gData
    gData.maxIter = maxIter
end


function plot_results()
    global gData
    
    ## Plot the thing
    Xhat = reshape(linspace(-10,10,200),:,1)
    Xhat = nnet.apply_scaler(Xhat, gData.scaler_X)
    yhat = nnet.predict(gData.w,
                             [ones(length(Xhat),1) Xhat],
                             gData.nHidden)

    Xhat = nnet.unapply_scaler(Xhat, gData.scaler_X)
    yhat = nnet.unapply_scaler(yhat, gData.scaler_y)

    X = gData.X
    y = gData.y

    figure(0)
    clf()
    plot(X,y,".")
    plot(Xhat,yhat,"g-")

    figure(1)
    clf()
    semilogy(gData.list_iter, gData.list_error, "b-")
end


function iterate()
    global gData
    
    # Load X and y variable
    X = copy(gData.X)
    y = copy(gData.y)
    Xtest = copy(gData.Xtest)
    ytest = copy(gData.ytest)
    n = size(X,1)
    
    ## Scale the data
    scaler_X = gData.scaler_X
    scaler_y = gData.scaler_y
    X = nnet.apply_scaler(X, scaler_X)
    y = nnet.apply_scaler(y, scaler_y)
    Xtest_scaled = nnet.apply_scaler(Xtest, scaler_X)

    ## Augment the data with 1
    X = [ones(n,1) X]
    d = 2
    
    # Choose network structure and randomly initialize weights
    nHidden = gData.nHidden
    nParams = nnet.nParams(d,nHidden)
    w       = copy(gData.w)

    ##
    ## Keep track of error
    list_iter   = copy(gData.list_iter)
    list_error  = copy(gData.list_error)

    # Train with stochastic gradient
    # maxIter = 100000
    #maxIter = 100000
    maxIter = gData.maxIter
    stepSize = gData.stepSize
    w_old = w
    for t in (gData.indexIter):(gData.indexIter+maxIter)    
        # The stochastic gradient update:
        i = rand(1:n)
        (f,g) = nnet.backprop(w,X[i,:],y[i],nHidden)
        w_new = w - stepSize*g + 0.9 * (w - w_old)
        w_old = w
        w = w_new

        # Every few iterations, plot the data/model:
        if (mod(t-1,1000) == 0)

            # calculate error
            ytest_scaled_hat = nnet.predict(w,[ones(length(Xtest_scaled),1) Xtest_scaled],nHidden)
            ytest_hat = nnet.unapply_scaler(ytest_scaled_hat, scaler_y)
            error = norm(ytest-ytest_hat)

            ## print error
	    @printf("Training iteration = %d, test error = %f \n",t-1, error)
            
            ## record error for ploting
            append!( list_error, error )
            append!( list_iter, t )
        end
    end
    

    ## SEt the results
    gData.w = w
    gData.list_error = list_error
    gData.list_iter = list_iter
    gData.indexIter = gData.indexIter+maxIter
end

end; ## module example_nnet
