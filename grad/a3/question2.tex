\section{Mixture Models and Expectation Maximization}


\subsection{Categorical Mixture Model}

Consider a density estimation with examples $x^i \in \{1,2,\dots,k\}^d$ representing a set of $d$ categorical variables. In this setting, a natural way to model an individual variable $x^i_j$ would be with a categorical distribution,
\[
p(x^i_j = c \cond \theta_{j,1}, \theta_{j,2}, \dots, \theta_{j,k}) = \theta_{j,c},
\]
so the joint distribution would be
\[
p(x^i \cond  \theta_{j,1}, \theta_{j,2}, \dots, \theta_{j,k}) = \prod_{j=1}^d \theta_{j,x^i_j},
\]
where all $\theta_{j,c} \geq 0$ and $\sum_{c=1}^k \theta_{j,c} = 1$ for each feature $j$.
However, if we assume this structure for each variable then the variables would be independent. One way to model dependent count variables would be with a mixture of $m$ independent categorical distributions,
\[
p(x^i\cond  \Theta) = \sum_{c=1}^{m}p(z^i = c\cond \Theta^t)p(x^i \cond z^i = c, \Theta^t) =  \sum_{c = 1}^m \pi_c \prod_{j=1}^d \theta_{j,x_j^i}^{c},
\]
where:
\items{
\item $\pi_c$ is the probability that the examples comes from mixture $c$, $p(z^i = c \cond \Theta) = \pi_c$. 
\item $\theta_{j,c'}^c$ is the probability that $x_j^i$ is $c'$ for examples from mixture $c$, $p(x_j^i = c' \cond z^i = c, \Theta)$.
\item We use $\Theta$ as the set containing all the $\pi_c$ and $\theta_{j,c'}^c$ values.
}
\blu{Derive the EM update for this mixture model} (treating the $z^i$ as missing values).

{\color{gre}
Let's establish some notation first. I will use $n$ to denote number of samples, $d$ number of features, $k$ the number of categories, and $m$ number of clusters. Furthermore, I will use dummy variables $i$, $j$, $s$, and $c$ to iterate over $[1 \, n]$, $[1 \, d]$, $[1 \, k]$, and $[1 \, m]$, respectively.  $x^i_j \in [1 \, k]$ is the $j$th category of the $i$th sample, $z^i \in [1 \, m]$ the cluster number of $i$th sample, $\theta_{jsc}=p(x_j=s \cond z=c)$, and $\pi_c = p(z=c)$. I gather all the $\theta$, $\pi$, and $x$ values in matrices $\Theta$, $\Pi$, and $X$, respectively, which have the dimensions shown in Figure \ref{fig:em-mat-dims}.
} %end of ans
\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\linewidth]{img/em-mat-dims}
  \caption{Dimension of matrices involved in EM}\label{fig:em-mat-dims}
\end{figure}

{\color{gre}
From the class slides, we know that the expectation minimization algorithm starts with an initial guess for $\Theta$ and $\Pi$. At each iteration, first values $\alpha^{i}_{c}$ are computed,
\begin{equation}
\label{eq:em-abstract-step1}    
  \alpha^{i}_{c}=p(z=c \cond x=x^i, \Theta, \Pi).
\end{equation}
Then, $\Theta, \Pi$ are updated as
\begin{equation}
\label{eq:em-abstract-step2}
\begin{aligned}
  \Theta, \Pi \gets & \argmin{\Theta, \Pi}\left(
    \text{NLL} = -\sum_{i} \sum_{c}
    \alpha^{i}_{c} \log \left( p(z=c,x=x^i \cond \Theta, \Pi) \right)
  \right) \\
  & \text{subject to } \sum_c{\pi_c}=1; \quad \pi_c>0;
  \quad \sum_s{\theta_{j,s,c}}=1; \quad  \theta_{j,s,c}>0.
\end{aligned}
\end{equation}
This procedure is continued until convergence is reached. Thus, ``all we have to do'' is to find explicit expressions for equations \eqref{eq:em-abstract-step1} and \eqref{eq:em-abstract-step2}.

For the first step we can use the Bayes rule
\[
  p(z=c \cond x=x^i, \Theta, \Pi)
  = \frac{
    p(z=c \cond \Theta, \Pi)p(x=x^i \cond z=c, \Theta, \Pi)
  }{
    p(x=x^i \cond \Theta, \Pi)
  }
  = \frac{
    \pi_c \prod_{j=1}^{d} \theta_{j,{x^i_j},c}
  }{
    \sum_{c'} \pi_{c'} \prod_{j=1}^{d} \theta_{j,{x^i_j},c'}
  },
\]
which gives us the explicit formula for the step 1 of the EM algorithm \\
\fbox{\parbox{0.99\linewidth}{\centering\begin{equation}
  \label{eq:em-explicit-step1}
  \alpha^{i}_{c}
  = \frac{
    \pi_c \prod_{j} \theta_{j,{x^i_j},c}
  }{
    \sum_{c'} \pi_{c'} \prod_{j} \theta_{j,{x^i_j},c'}
  }.
\end{equation}}}

The second step is slightly more complicated. First, let's simplify the expression for negative log likelihood (NLL).
\[
  \text{NLL} =-\sum_{i} \sum_{c}
  \alpha^{i}_{c} \log \left( p(z=c,x=x^i \cond \Theta, \Pi) \right) \\
  %%
  =-\sum_{i} \sum_{c} \alpha^{i}_{c} \left( \sum_{j}  \log\left( \theta_{j,{x^i_j},c} \right) + \log(\pi_c) \right)
\]
Defining intermediate variables
$\boxed{p_{jsc}=\sum_i \mathcal{I}[x^i_j=s]\alpha^{i}_{c}}$,
$\boxed{q_{c}=\sum_i \alpha^{i}_{c}}$
we can further simplify the NLL to
\begin{equation}
  \label{eq:em-decouple-pi-theta}
  \text{NLL}  =
  - \sum_j\sum_s\sum_c p_{jsc} \log(\theta_{jsc})
  -\sum_c q_c \log(\pi_c).
\end{equation}
As the terms including $\Theta$ and $\Pi$ in equation \eqref{eq:em-decouple-pi-theta} are decoupled, we can minimize them separately. For $\pi$ we are looking for 
\[
  \begin{aligned}
    & \argmin{\Pi}\left( f(\Pi) \right) \text{ subject to } g(\Pi)=0 \\
    & f \in \R; \quad f = -\sum_c q_c \log (\pi_c) \\
    & g \in \R; \quad g = \sum_c \pi_c - 1 
  \end{aligned}
\]
Using lagrange multipliers, the optimality conditions would then be
\[
\begin{cases}
  &\nabla_\Pi f + \lambda \nabla_\Pi g = 0 \\
  & g = 0
  \end{cases} \Rightarrow
  \begin{cases}
  & -\frac{q_c}{\pi_c} + \lambda  = 0 \;\; \forall c \\
  & \sum_c \pi_c = 1
\end{cases}
\]
where $\lambda \in \R$ is the Lagrange multiplier. It is trivial to see that the solution is $\boxed{\pi_c = \frac{q_c}{\sum_{c'} q_{c'}}}$ which also \emph{luckily} statisfies the non-negativity condition.

For $\Theta$ we wish to solve the following problem.
\[
  \begin{aligned}
    & \argmin{\Theta}\left( f(\Theta) \right) \text{ subject to } g(\Theta)=0 \\
    & f \in \R; \quad\quad\;\;\; f = - \sum_j\sum_s\sum_c p_{jsc} \log(\theta_{jsc}) \\
    & g \in \R^{d \times m}; \quad g_{jc} = \sum_s \theta_{jsc} - 1 
  \end{aligned}
\]
Using lagrange multipliers, the optimality conditions would then be
\[
\begin{cases}
  &\nabla_\Theta f + \sum_{j'} \sum_{c'} \lambda_{j'c'}  \nabla_\Theta g_{j'c'} = 0 \\
  & g = 0
  \end{cases} \Rightarrow
  \begin{cases}
  & -\frac{p_{jsc}}{\theta_{jsc}} + \lambda_{jc}  = 0 \quad \forall j,s,c \\
  & \sum_s  \theta_{j,s,c} = 1 \quad \forall j,c
\end{cases}
\]
where $\lambda \in \R^{d \times m}$ is the matrix of Lagrange multipliers. It is trivial to see that the solution is $\boxed{\theta_{jsc} = \frac{p_{jsc}}{\sum_{s'}p_{js'c}}}$ which also \emph{luckily} statisfies the non-negativity condition.
} %end of ans

\newpage
\subsection{Gaussian Mixture Model Implementation}

The script \emph{example\_gaussian} fits a Gaussian distribution to a dataset that is not uni-modal, and giving a bad fit. Implement the EM for fitting a mixture of Gaussians to this data. \blu{Hand in your code and the updated plot when using a mixture of 3 Gaussians}.

Hint: you may want to start by implementing the PDF (``predict'') function; you can then use this with the monotonicity of EM to debug your implementation.

It is possible that $\Sigma_k^{t+1}$ may not be positive-definite, if you encounter this the standard fixes are to remove such clusters or use a MAP estimate for $\Sigma$ where you add a small multiple of the identity matrix to the estimate.

{\color{gre}
Let's first write down the required formulas, using the same notation as question 2.1. The new symbols are $\boldsigma$ for the variance parameters, and $\boldmu$ for the mean parameters,  $\pi$ for cluster probabilities, and $p^i_c$ which will be introduce shortly. The probability distributions are
\[\begin{aligned}
    & p(x=x^i \cond z=c, \boldsigma, \boldmu, \Pi) = p^i_c =  \frac{1}{(2\pi)^{d/2}|\boldsigma_c|^{1/2}}\exp(\frac{-1}{2}(x^i-\boldmu)\boldsigma_c^{-1}(x^i-\boldmu_c)) \\
    & p(z=c \cond \boldsigma, \boldmu, \Pi) = \pi_c \\
  \end{aligned}\]
So the $\alpha^i_c$ values can be computed as
\[
  \alpha^i_c
  = \frac{
    p(z=c \cond \boldsigma, \boldmu, \Pi)p(x=x^i \cond z=c, \boldsigma, \boldmu, \Pi)
  }{
    p(x=x^i \cond \boldsigma, \boldmu, \Pi)
  }
  = \frac{
    \pi_c p^i_c
  }{
    \sum_{c'} \pi_{c'} p^i_{c'}
  }.
\]
The NLL in this problem takes the form
\[
  \text{NLL} =-\sum_{i} \sum_{c}
  \alpha^{i}_{c} \log \left( p(z=c,x=x^i \cond \boldmu, \boldsigma, \Pi) \right) \\
  %%
  =-\sum_{i} \sum_{c} \alpha^{i}_{c}   \left( \log( p^i_c ) + \log(\pi_c) \right).
\]
Again, by defining 
\[
  q_{c}=\sum_i \alpha^{i}_{c},
\]
the $\pi$ values can be found as
\[
  \pi_c = \frac{q_c}{\sum_{c'} q_{c'}}.
\]
For computing $\boldsigma_c$ and $\boldmu_c$, we see that the optimization is independant for each cluster $c$. The only difference is that the samples are now weighted by the term $\alpha^i_c$. Therefore, $\boldsigma_c$ and $\boldmu_c$ are computed very similar to the single Gaussian case 
\[ \begin{aligned}
    & \boldmu_c = \frac{\sum_i(\alpha^i_c x^i)}{\sum_i \alpha^i_c}, \\
    & \boldsigma_c = \frac{1}{\sum_i \alpha^i_c} \sum_i \alpha^i_c(x^i - \mu_c)(x^i - \mu_c)^T. \\
\end{aligned}\]
} % End of ans

The solution using three Gaussians and the code are given below.

\begin{center}
  \includegraphics[width=0.45\linewidth]{img/em-gaussian-init.pdf} \hfil
  \includegraphics[width=0.45\linewidth]{img/em-gaussian.pdf}  \\
  \makebox[0.45\linewidth][c]{initial guess} \hfil
  \makebox[0.45\linewidth][c]{converged solution ($\Pi = [0.29, 0.39, 0.32]$)}  \\
\end{center}

\begin{lstlisting}%
[frame=single, caption={Fitting Mixture of Gaussians}]

# ======================================================
# Internal stuff
# ======================================================

# The parameters of a Gaussian
struct GaussianParams
  mu::VecFloat
  Sigma::MatFloat
  SigmaInv::MatFloat
end

# Fit a single Gaussian to data with weights
function _fitSingleGaussian(X::MatFloat, weights::VecFloat=VecFloat([]))
  (n,d) = size(X)

  # Create weights if they are not there
  if( length(weights[:])==0 )
    weights = ones(n)
  end
  @assert( length(weights[:]) == n )
  weights[:] = weights[:] / sum(weights[:])

  mu = sum(weights .* X,dims=1)'
  Xc = X - repeat(mu',n)
  eps = 1e-10 # tiny regularization
  Sigma = (Xc' * (weights .* Xc) + eps*eyeMat(d))
  SigmaInv = Sigma^-1

  return GaussianParams(mu[:], Sigma, SigmaInv)
end

# Evaluate single Gaussian
function _evalSingleGaussian(p::GaussianParams, Xhat::MatFloat, performExp=false)
  (t,d) = size(Xhat)
  PDFs = zeros(t)

  logZ = (d/2)log(2pi) + (1/2)logdet(p.Sigma)  
  for i in 1:t
    xc = Xhat[i,:] - p.mu
    loglik = -(1/2)dot(xc,p.SigmaInv*xc) - logZ
    if(performExp)
      PDFs[i] = exp(loglik)
    else
      PDFs[i] = loglik
    end
  end

  return PDFs  
end

# ======================================================
# Fit a mixture of Gaussians
# ======================================================

# X the data
# numGaussians: number of Gaussians to fit
# maxIter: maximum number of EM iterations
function gaussianMixtureDensity(X::MatFloat, numGaussians::Int; maxIter=1)

  # Fix seed for debugging
  seed = MersenneTwister(324523)
  
  # initial guess
  (n,d) = size(X)
  m = numGaussians
  #
  sigma = repeat(reshape(eyeMat(d),(d,d,1)), outer = [1,1,m])
  sigmaInv = copy(sigma)
  mu = randn(seed, d,m)*5
  pi = ones(m) / m
  logp = zeros(n,m)
  alpha = zeros(n,m)

  # find values of each gaussian
  function _evallogp(_X, _mu, _sigma, _sigmaInv)
    _logp = zeros(size(_X,1),m)    
    for c =1:m
      p = GaussianParams(_mu[:,c], _sigma[:,:,c], _sigmaInv[:,:,c])
      _logp[:,c] = _evalSingleGaussian(p, _X, false)
    end

     return _logp
  end

  # Find alpha values
  function _evalalpha(_logp, _pi)
    # scaling that won't affect final result, just better numerics
    scaled_logp = _logp .- maximum(_logp, dims=2)
    p = exp.(scaled_logp)
    # compute
    pi_p = _pi' .* p
    _alpha = pi_p ./ sum(pi_p, dims=2)

    return _alpha
  end

  # Optimize for each Gaussian
  function _optimGaussians(_X, _alpha)
    @assert(size(X,1) == size(_alpha,1))
    
    _sigma = zeros(size(sigma))
    _sigmaInv = zeros(size(sigma))
    _mu = zeros(size(mu)) 
    _pi = zeros(size(mu)) 

    for c =1:m
      p = _fitSingleGaussian(_X, _alpha[:,c])
      _sigma[:,:,c] = p.Sigma
      _sigmaInv[:,:,c] = p.SigmaInv
      _mu[:,c] = p.mu
    end

    q = sum(_alpha,dims=1)
    _pi = q[:] / sum(q[:])

    return (_pi, _mu, _sigma, _sigmaInv)
  end

  # Probability distribution
  function _PDF(_Xhat)
    t = size(_Xhat,1)
    @assert(size(_Xhat,2) == d)
    logp = _evallogp(_Xhat, mu, sigma, sigmaInv)
    p = exp.(logp)
    return sum(pi' .* p, dims=2)
  end

  # Perform the optimization
  @printf("%20s %20s \n", "iter", "param_change")
  for i=1:maxIter
    logp = _evallogp(X, mu, sigma, sigmaInv)
    alpha = _evalalpha(logp, pi)
    (piNew, muNew, sigmaNew, sigmaInv) = _optimGaussians(X, alpha)
    #    
    paramChange = norm([piNew[:]-pi[:]; muNew[:]-mu[:]; sigmaNew[:]-sigma[:]])
    #
    pi = piNew
    mu = muNew
    sigma = sigmaNew

    # report progress
    @printf("%20g %20g \n", i, paramChange)

    # check convergence
    if( paramChange < 1e-6 )
      break
    end
  end
  
  # return the model
  return DensityModel(_PDF)
end
\end{lstlisting}

%% Emacs latex configurations; please keep at the end of the file.
%% Local Variables:
%% TeX-master: "root"
%% End: 
