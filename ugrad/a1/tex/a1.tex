%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            INCLUDES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[‎oneside‎,11pt]{article}

\usepackage[fleqn]{amsmath}
\usepackage{fullpage}
\usepackage{xcolor}
\usepackage{url}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{amssymb}
\usepackage{nicefrac}
\usepackage{algorithm2e}
\usepackage{float}
\usepackage{fancyvrb}
\usepackage{tikz}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            MACROS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  USE LIBERTINE
\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono
\setlength{\mathindent}{0pt}

%% hyperref
\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.2 0.6 0.6},%
linktocpage%
]{hyperref}


% Colors
\definecolor{blu}{rgb}{0.2,0.0,0.7}
\def\blu#1{{\color{blu}#1}}
\definecolor{gre}{rgb}{0,.5,0}
\def\gre#1{{\color{gre}#1}}
\definecolor{red}{rgb}{1,0,0}
\def\red#1{{\color{red}#1}}
\def\norm#1{\|#1\|}

% Math
\def\R{\mathbb{R}}
\def\argmax{\mathop{\rm arg\,max}}
\def\argmin{\mathop{\rm arg\,min}}
\newcommand{\mat}[1]{\begin{bmatrix}#1\end{bmatrix}}
\newcommand{\alignStar}[1]{\begin{align*}#1\end{align*}}

% LaTeX
\newcommand{\fig}[2]{\includegraphics[width=#1\textwidth]{a1f/#2}}
\newcommand{\centerfig}[2]{\begin{center}\includegraphics[width=#1\textwidth]{a1f/#2}\end{center}}
\def\items#1{\begin{itemize}#1\end{itemize}}
\def\enum#1{\begin{enumerate}#1\end{enumerate}}
\newcommand{\matCode}[1]{\lstinputlisting[language=Matlab]{a1f/#1.m}}

\DefineVerbatimEnvironment{ColorVerbatim}{Verbatim}%
  {formatcom=\color{blu},commandchars=\\\{\}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%                            DOCUMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{CPSC 340 Assignment 1}
\author{Shayan Hoshyari, 81382153 \\
Hooman Shariati, 21155098}
\date{}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%              Summary Statistics and Data Visualization
%%                           ME 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Summary Statistics and Data Visualization}

Download and expand the file \emph{a1.zip}, which contains estimates
of the influenza-like illness percentage over 52 weeks on 2005-06 by
Google Flu Trends in a comma-separated values (CSV) file. You can open
this with Excel or other spreadsheet programs; the first row gives the
abbreviation of the region names for each column, and each row gives
the estimate for a week.  After you change to the a0 directory, you
can load this data in Julia using:
\begin{verbatim}
dataTable = readcsv("fluTrends.csv")
\end{verbatim}
This creates an two-dimensional array of type ``Any'' populated with
all the information in the CSV file.

%%-------------------------------------------------------------------------
%%                        Summary Statistics
%%-------------------------------------------------------------------------
\subsection{Summary Statistics}

Report the following statistics: the minimum, maximum, mean, median,
and mode of all values across the dataset. In light of thea above, is
the mode a reliable estimate of the most ``common'' value?  Describe
another way we could give a meaningful ``mode'' measurement for this
(continuous) data.


\blu{%
  Mode is certainly not a good measure, since checking equality for
  real numbers is not a good idea. We could use a step size
  $\epsilon$, and then round all the numbers to the closest
  $k\epsilon,k \in \mathcal N$. Then the mode of the preprocessed
  values can be found. Alternatively, we can fit a probability
  distribution to the entries, and find the $\argmax$ of the
  distribution.%
}


\begin{table}[H]
  \color{blu}
  \centering
  \begin{tabular}{cccccc}
    \hline
    feature \#      &min        &max       &mean     &median       &mode  \\
    \hline
    1    &0.42800    &2.31000    &1.22335    &1.12950      &0.428 \\
    2    &0.48300    &2.20500    &1.23354    &1.11600      &0.490 \\
    3    &0.45200    &2.51500    &1.27527    &1.26500      &0.452 \\
    4    &0.46400    &3.11500    &1.46021    &1.27750      &0.505 \\
    5    &0.46800    &2.71400    &1.29883    &1.10250      &0.542 \\
    6    &0.55400    &3.85900    &1.56252    &1.41650      &0.554 \\
    7    &0.45600    &3.21900    &1.29235    &1.10750      &0.499 \\
    8    &0.35200    &4.86200    &1.27002    &0.97850      &0.352 \\
    9    &0.37700    &2.66000    &1.06321    &0.95700      &0.377 \\
    10    &0.60600    &3.26000    &1.56696    &1.30350      &0.715 \\
    All entries    &0.35200    &4.86200    &1.32462    &1.15900      &0.770  \\
    \hline
  \end{tabular}
\end{table}

%%-------------------------------------------------------------------------
%%                        Data Visualization
%%-------------------------------------------------------------------------
\subsection{Data Visualization}

Consider the figure on the next page.  The figure contains the
following plots, in a shuffled order:
\begin{enumerate}
\item%
  A histogram showing the distribution of each the values in the
  matrix $X$. \\
  \blu{ $\Rightarrow$~Plot~C. All the values are gathered together. }
\item%
  A boxplot grouping data by weeks, showing the distribution across
  regions for each week. \\
  \blu{ $\Rightarrow$~Plot~B. Distribution of number of ill people per
    week. }
\item%
  A scatterplot between the two regions with highest correlation. \\
  \blu{ $\Rightarrow$~Plot~F. There is a perfect linear relationship.}
\item%
  A single histogram showing the distribution of \emph{each}
  column in $X$. \\
  \blu{ $\Rightarrow$~Plot~D. Every column is shown.}
\item%
  A scatterplot between the two regions with lowest correlation. \\
  \blu{ $\Rightarrow$~Plot~E. No linear or non-linear relationship.}
\item%
  A plot containing the weeks on the $x$-axis and the percentages
  for each region on the $y$-axis. \\
  \blu{ $\Rightarrow$~Plot~A. No need to explain!}
\end{enumerate}

Match the plots (labeled A-F) with the descriptions above (labeled
1-6), with an extremely brief (a few words is fine) explanation for
each decision.

%%-------------------------------------------------------------------------
%%                          Decision Surfaces
%%-------------------------------------------------------------------------
\subsection{Decision Surfaces}

Consider the figure below, which plots a set of two-dimensional
training examples and the decision surface produced by a ``neural
network'' classifier (a model we'll see later in the course).  How
many training examples has the neural network mis-classified?

\blu{14 definite cases and one ambiguous case.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%                             Decision Trees
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Decision Trees}

If you run the file \emph{example\_decisionStump.jl}, it will load a
dataset containing longtitude and latitude data for 400 cities in the
US, along with a class label indicating whether they were a ``red''
state or a ``blue'' state in the 2012 election. Specifically, the first
column of the variable $X$ contains the longitude and the second
variable contains the latitude, while the variable $y$ is set to $1$
for blue states and $2$ for red states. After it loads the data, it
plots the data and then fits two simple classifiers: a classifier that
always predicts the most common label ($1$ in this case) and a
decision stump that discretizes the features (by rounding to the
nearest integer) and then finds the best equality-based rule (i.e.,
check if a feature is \emph{equal} to some value). It reports the
training error with these two classifiers, then plots the decision
areas made by the decision stump.

%%-------------------------------------------------------------------------
%%                  Equality vs. Inequality Splitting Rules        
%%-------------------------------------------------------------------------
\subsection{Equality vs. Inequality Splitting Rules}

In class we discussed splitting rules based on inequalities rather
than equalities. Is there a type of feature where it makes sense
to use an equality-based splitting rule?

\blu{ It might be useful for features that can attain only a few
  \emph{discrete} values. }

%%-------------------------------------------------------------------------
%%                   Decision Stump Implementation     
%%-------------------------------------------------------------------------
\subsection{Decision Stump Implementation}

The file \emph{decision\_stump.jl} contains a function that finds the
best decision stump using the equality rule
(``decisionStumpEquality''), and then returns a function that can
apply this decision stump to new data. Instead of discretizing the
data and using a rule based on testing an equality for a single
feature, we want to check whether a feature is above a threshold and
split the data accordingly (this is the more sane approach, which we
discussed in class). Add a new function ``decisionStump'' to
\emph{decision\_stump.jl} that finds the best inequality-based rule,
and report the updated error you obtain by using inequalities instead
of discretizing and testing equality.

\blu{ The code is handed-in.}

\begin{ColorVerbatim}[frame=single]
Error with majority predictor:             0.41
Error with equality-rule decision stump:   0.38
Error with inequality-rule decision stump: 0.25
\end{ColorVerbatim}

\begin{ColorVerbatim}[frame=single]
function decisionStump(X,y)
    # Fits a decision stump based on equality after rounding to
    # nearest integer
    # Yes and no are compeletely conventional.
    # Only needed four changes. Remove two mods, conver == to >= .

    # Get the size of the data matrix
    (n,d) = size(X)

    # Initial the best rule with the baseline rule (no split)
    y_mode = mode(y)
    minError = sum(y .!= y_mode);
    splitVariable = [];
    splitValue = [];
    splitYes = y_mode;
    splitNo = [];


    # Search for the best rule
    # (Uses O(n^2d) approach to keep code simple)
    yhat = zeros(n)
    for j in 1:d
	# Try unique values of column as split values
	for val in unique(X[:,j])

	    # Test whether each object satisfies equality
	    yes = X[:,j] .>= val
	    
	    # Find correct label on both sides of split
	    y_yes = mode(y[yes])
	    y_no = mode(y[.!yes])

	    # Make predictions
	    yhat[yes] = y_yes
	    yhat[.!yes] = y_no

	    # Compute error
	    trainError = sum(yhat .!= y)

	    # Update best rule
	    if trainError < minError
		minError = trainError
		splitVariable = j
		splitValue = val
		splitYes = y_yes
		splitNo = y_no
	    end
	end
    end

    # Now that we have the best rule,
    # let's build our splitting function
    function split(Xhat)
	(t,d) = size(Xhat)
	if isempty(splitVariable)
	    return fill(true,t)
	else
	    return (Xhat[:,splitVariable] .>= splitValue)
	end
    end

    # Now that we have the best rule,
    # let's build our predict function
    function predict(Xhat)
	(t,d) = size(Xhat)
	yes = split(Xhat)
	yhat = fill(splitYes,t)
	if any(.!yes)
	    yhat[.!yes] = splitNo
	end
	return yhat
    end
    
    return StumpModel(predict,split,isempty(splitNo))
end
\end{ColorVerbatim}

%%-------------------------------------------------------------------------
%%                   Constructing Decision Trees     
%%-------------------------------------------------------------------------
\subsection{Constructing Decision Trees}

Once your \emph{decisionStump} function is finished, the script
\emph{example\_decisionTree} will be able to fit a decision tree of
depth 2 to the same dataset (which results in a lower training
error). Look at how the decision tree is stored and how the
(recursive) \emph{predict} function works. Using the same splits as
the fitted depth-2 decision tree, write out what an alternate version
of the predict function would be for classifying one training example
as a simple program using if/else statements (as in slide 7 of L3).

\begin{algorithm}[H]
  \color{blu}
  Given the sample $x=[x_1,x_2]^T$, we will find $\hat{y}$\;%
  \eIf{$x_2 \geq 37.695$}% YES CHILD
  {%
    \eIf{$x_1 \geq -96.032$}% YES
    {%
      $\hat y = 1$
    }{% ELSE NO
      $\hat y = 2$
    }%
  }{% ELSE, NO CHILD
    \eIf{$x_2 \geq -112.548$}% YES
    {%
      $\hat y = 2$
    }{% ELSE NO
      $\hat y = 1$
    }%
  }%
\end{algorithm}


%%-------------------------------------------------------------------------
%%                   Cost of Fitting Decision Trees     
%%-------------------------------------------------------------------------
\subsection{Cost of Fitting Decision Trees}

In class, we discussed how in general the decision stump minimizing
the classification error can be found in $O(nd\log n)$ time. Using the
greedy recursive splitting procedure, what is the total cost of
fitting a decision tree of depth $m$ in terms of $n$, $d$, and $m$?

\blu{ Cost of fitting a tree to $n$ data points with dimension $d$ is
  $O(nd)$. Suppose at a certain depth we have $t$ leaves (each with
  $n_i$ member points) in our tree. Obviously, $ \sum_{i=1}^t n_i=n$.
  The cost of going one level deeper will then be:
  $ \sum_{i=1}^t O(n_id) = O(nd) $. Therefore, the cost of going to a
  certain depth $m$ would be $mO(nd)=O(mnd)$. }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%                             Decision Trees
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Training and Testing}

%%-------------------------------------------------------------------------
%%                            Traning Error 
%%-------------------------------------------------------------------------

\subsection{Traning Error}

Running \texttt{example\_train.jl} fits decision trees of different
depths using two different implementations: first, our own
implementation using your ``decisionStump'' function, and also using a
variant using a more sophisticated splitting criterion called the
information gain. Describe what you observe. Can you explain the
results?

\blu{ As shown in Fig.~\ref{fig:infogain}, our method is not able to
  reduce the training error below a certain threshold. The training
  error is not the right metric to judge the classifier with. However,
  a classifier that cannot reduce the training error, is unlikely to
  reduce the test error either.  Also, for smaller depths our method
  seems to be working better.}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.45\linewidth]{img/infogain}
  \caption{Training error vs. tree depth for different error metrics.}
  \label{fig:infogain}
\end{figure}

\subsection{Training and Testing Error Curves}

Notice that the \emph{citiesSmall.mat} file also contains test data,
``Xtest" and ``ytest".  Running \emph{example\_trainTest} trains a
depth-2 decision tree and evaluates its performance on the test data.
With a depth-2 decision tree, the training and test error are fairly
close, so the model hasn't overfit much.

Make a plot that contains the training error and testing error as you
vary the depth from 1 through 15. How do each of these errors change
with the decision tree depth?

\blu{ As shown in Fig.~\ref{fig:infogain2}, The training error keeps
  dropping, but the test error reaches ay plateau (except for a minor
  increase from depth $8$ to $9$). It is interesting that we are not
  overfitting that much even for depth $20$.}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.45\linewidth]{img/infogain2}
  \caption{Training and test error for the infogain-based tree. An
    explicit test set is used.}
  \label{fig:infogain2}
\end{figure}



\subsection{Validation Set}

Suppose we're in the typical case where we don't have the labels for
the test data. In this case, we might instead use a \emph{validation}
set. Split the training set into two equal-sized parts: use the first
$n/2$ examples as a training set and the second $n/2$ examples as a
validation set (we're assuming that the examples are already in a
random order).

What depth of decision tree would we pick if we minimized the
validation set error?

\blu{ As shown in Fig.~\ref{fig:infogain3}, the depth 3 seems to be a
  good choice. Since for depths bigger than three, the validation
  error starts increasing.}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.45\linewidth]{img/infogain3}
  \caption{Training and test error for the infogain-based tree. The
    second half of the initial training data is excluded and used as
    the test set.}
  \label{fig:infogain3}
\end{figure}

Does the answer change if you switch the training and validation set?

\blu{ Yes, but only slightly. As shown in Fig.~\ref{fig:infogain4},
  depth 6 might be a better choice if we were to only look at this
  data.}
\begin{figure}[H]
  \centering
  \includegraphics[width=0.45\linewidth]{img/infogain4}
  \caption{Training and test error for the infogain-based tree. The
    first half of the initial training data is excluded and used as
    the test set.}
  \label{fig:infogain4}
\end{figure}

How could we use more of our data to estimate the depth more reliably?

\blu{ We can do a $k$-fold cross-validation. The data will be divided
  into $k$ different folds, where each fold contains a validation and
  a training set. For each set of hyper-parameters, $k$ different
  classifiers are fit to the training set of each fold. Then each
  classifier is used to find the $\hat y$ of its corresponding
  validation set. Then all the $\hat y$ values are used together to
  find a single \emph{score} or \emph{error} value. The set of
  hyper-parameters is chosen that maximizes this validation score. }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%                        Naive Bayes
%%                           Hooman 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Naive Bayes}

In this section we'll implement naive Bayes, a very fast
classification method that is often surprisingly accurate for text
data with simple representations like bag of words.



%%-------------------------------------------------------------------------
%%                        Naive Bayes by Hand
%%-------------------------------------------------------------------------
\subsection{Naive Bayes by Hand}

Consider the dataset below, which has $10$ training examples and $2$ features:
\[
X = \begin{bmatrix}0 & 1\\1 & 1\\ 0 & 0\\ 1 & 1\\ 1 & 1\\ 0 & 0\\  1 & 0\\  1 & 0\\  1 & 1\\  1 &0\end{bmatrix}, \quad y = \begin{bmatrix}1\\1\\1\\1\\1\\1\\0\\0\\0\\0\end{bmatrix}.
\]
Suppose you believe that a naive Bayes model would be appropriate for
this dataset, and you want to classify the following test example:
$ \hat{x} = \begin{bmatrix}1 & 0\end{bmatrix}. $

(a) Compute the estimates of the class prior probabilities (you
don't need to show any work):
\begin{itemize}
\item$ p(y = 1)$ \blu{$ =60\%.$}
\item $p(y = 0)$ \blu{$ =40\%.$}
\end{itemize}

(b) Compute the estimates of the 4 conditional probabilities required by naive Bayes for this example  (you don't need to show any work):
\begin{itemize}
\item $p(x_1 = 1 | y = 1)$ \blu{$=50\%.$}
\item $p(x_2 = 0 | y = 1)$ \blu{$=33.3\%.$}
\item $p(x_1 = 1 | y = 0)$ \blu{$=100\%.$}
\item $p(x_2 = 0 | y = 0)$ \blu{$=75\%.$}
\end{itemize}

(c) Under the naive Bayes model and your estimates of the above
probabilities, what is the most likely label for the test example?
(Show your work.) \\
\blu{
  \[
    p(\hat{y}=1 | \hat{x}) \approx%
    p(\hat{x_1}=1|\hat{y}=1 )  p(\hat{x_2}=0|\hat{y}=1 ) p(\hat{y}=1)= %
    \frac{1}{2}*\frac{1}{3}*\frac{3}{5} = 0.1%
  \]
  \[
    p(\hat{y}=0 | \hat{x}) \approx%
    p(\hat{x_1}=1|\hat{y}=0 ) p(\hat{x_2}=0|\hat{y}=0 ) p(\hat{y}=0)= %
    1*\frac{3}{4}*\frac{2}{5} = 0.3%
  \]
  Since $p(\hat{y}=0| \hat{x})>p(\hat{y}=1|\hat{x})$ the most likely
  label for $\hat{x} = \begin{bmatrix}1 & 0\end{bmatrix}$ is $0$.
}

%%-------------------------------------------------------------------------
%%                              Bag of Words
%%-------------------------------------------------------------------------
\subsection{Bag of Words}

If you run the script \emph{example\_BagOfWords.jl}, it will load the
following dataset:
\begin{enumerate}
\item $X$: A sparse binary matrix. Each row corresponds to a newsgroup
  post, and each column corresponds to whether a particular word was
  used in the post. A value of $1$ means that the word occured in the
  post.
\item $wordlist$: The set of words that correspond to each column.
\item $y$: A vector with values $1$ through $4$, with the value
  corresponding to the newsgroup that the post came from.
\item $groupnames$: The names of the four newsgroups.
\item $Xvalidate$ and $yvalidate$: the word lists and newsgroup labels
  for additional newsgroup posts.
\end{enumerate}

Answer the following:
\begin{enumerate}
\item Which word is present in the newsgroup post if there is a $1$ in
  column 50 of X?  \blu{``league''.}
\item Which words are present in training example 500? 
  \blu{``car'', ``engine'', ``evidence'', ``problem'', ``system''.}
\item Which newsgroup name does training example 500 come from?
  \blu{newsgroup 2.}
\end{enumerate}

%%-------------------------------------------------------------------------
%%                     Naive Bayes Implementation
%%-------------------------------------------------------------------------
\subsection{Naive Bayes Implementation}

If you run the function \emph{example\_decisionTree\_newsgroups.jl} it
will load the newsgroups dataset and report the test error for
decision trees of different sizes (it may take a while for the deeper
trees, as this is a sub-optimal implementation). On other other hand,
if you run the function \emph{example\_naiveBayes.jl} it will fit the
basic naive Bayes model and report the test error.

While the \emph{predict} function of the naive Bayes classifier is
already implemented, the calculation of the variable $p\_xy$ is
incorrect (right now, it just sets all values to $1/2$). Modify this
function so that \emph{p\_xy} correctly computes the conditional
probabilities of these values based on the frequencies in the data
set. Hand in your code and report the test error that you obtain.

\blu{Test error with naive Bayes: 0.188. Code is handed in.}

\begin{ColorVerbatim}[frame=single]
  p_xy = zeros(2,d,k);
  xCounts = zeros(2,d,k);
  yCounts = zeros(k);

  for i in 1:n
    
    for j in 1:d
      if X[i,j]==1
        xCounts[1,j,y[i]] += 1;
      else
        xCounts[2,j,y[i]] += 1;
      end
    end

    yCounts[y[i]] += 1;
  end

  for i in 1:k
    p_xy[:,:,i] = xCounts[:,:,i] ./ yCounts[i];
  end
\end{ColorVerbatim}
%%-------------------------------------------------------------------------
%%               Runtime of Naive Bayes for Discrete Data
%%-------------------------------------------------------------------------
\subsection{Runtime of Naive Bayes for Discrete Data}

Assume you have the following setup:
\items{
\item The training set has $n$ objects each with $d$ features.
\item The test set has $t$ objects with $d$ features.
\item Each feature can have up to $c$ discrete values (you can assume $c \leq n$).
\item There are $k$ class labels (you can assume $k \leq n$) } You can
implement the training phase of a naive Bayes classifier in this setup
in $O(nd)$, since you only need to do a constant amount of work for
each $X(i,j)$ value. (You do not have to actually implement it in this
way for the previous question, but you should think about how this
could be done). What is the cost of classifying $t$ test examples
  with the model?

\blu{$O(tdk)$} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                        K-Nearest Neighbours
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{K-Nearest Neighbours}


In \emph{citiesSmall} dataset, nearby points tend to receive the same
class label because they are part of the same state. This indicates
that a $k-$nearest neighbours classifier might be a better choice than
a decision tree (while naive Bayes would probably work really badly on
this dataset). The file \emph{knn.jl} has implemented the training
function for a $k-$nearest neighbour classifier (which is to just
memorize the data) but the predict function always just predicts 1.

%%-------------------------------------------------------------------------
%%                        KNN Prediction
%%-------------------------------------------------------------------------

\subsection{KNN Prediction}

Fill in the \emph{predict} function in \emph{knn.jl} so that the model file implements the k-nearest neighbour prediction rule. You should use Euclidean distance, and  you may find it useful to pre-compute all the distances and then use the ``sortperm'' command.

\begin{enumerate}
\item%
  Hand in the predict function.

  \blu{Done. Can be found in \texttt{.jl} files.}

\begin{ColorVerbatim}[frame=single]
 function knn_predict(Xhat,X,y,k)
  (n,d) = size(X)
  (t,d) = size(Xhat)
  k = min(n,k) # To save you some debuggin

  output = zeros(t);
 
  for j in 1:t	
	distances = zeros(n);
	for i in 1:n
	   distances[i] = sqrt(sum(abs2.(X[i:i,:]-Xhat[j:j,:])));
	end
	sortedDistancesIndices = sortperm(distances);
	output[j] = mode(y[sortedDistancesIndices[1:k]]);
  end

  return output
end
\end{ColorVerbatim}
  
\item%
  Report the training and test error obtained on the
  \emph{citiesSmall.mat} dataset for $k=1$, $k=3$, and $k=10$. (You
  can use \emph{example\_knn.jl} to get started.)

  \blu {%
    For K = 1: \\
    Train Error with 1-nearest neighbours: 0.000 \\
    Test Error with 1-nearest neighbours: 0.065 \\
    For K = 3: \\
    Train Error with 3-nearest neighbours: 0.028 \\
    Test Error with  3-nearest neighbours: 0.066 \\
    For K = 10: \\
    Train Error with 10-nearest neighbours: 0.072 \\
    Test Error with 10-nearest neighbours: 0.097 %
  }
\item%
  Hand in the plot generatied by {classifier2Dplot} on the
  \emph{citiesSmall.mat} dataset for $k=1$ on the training data. (Note
  that this version of the function also plots the test data.)
  
   \blu{The plot is provided in Fig.~\ref{fig:knnvalid}.}
\item%
  Why is the training error $0$ for $k=1$?

  \blu {Because the closest neighbour to each point in the training
    set is itself and with k=1 we return the label of each training
    set as its prediction. (i.e. we are only comparing each point to
    itself and returning its label as our prediction). Therefore,
    there cannot be any prediction errors in the training set.}
\item%
  If you didn't have an explicit test set, how would you choose $k$?
  
  \blu {I would use cross-validation to find the best value for k. I
    would use 5-fold cross-validation, in which, I would train on 80\%
    of the data and validate on on the remaining 20\% (i.e. I would
    predict the labels for the 20\% of data using the nearest
    neighbours in the other 80\% ). I would do this 5 times, for each
    value of k, then average the results. I would choose the k-value
    with the lowest average error.}
\end{enumerate}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\linewidth]{img/output_1_0.png}
  \caption{Answer for problem 5.1.3.}  \label{fig:knnvalid}
\end{figure}


%%-------------------------------------------------------------------------
%%                     Condensed Nearest Neighbours
%%-------------------------------------------------------------------------

\subsection{Condensed Nearest Neighbours}

The file \emph{citiesBig1.mat} contains a version of this dataset with
more than 30 times as many cities. KNN can obtain a lower test error
if it's trained on this dataset, but the prediction time will be very
slow. A common strategy for applying KNN to huge datasets is called
\emph{condensed nearest neighbours}, and the main idea is to only
store a \emph{subset} of the training examples (and to only compare to
these examples when making predictions). A simple variation of this
algorithm would be:

\begin{algorithm}[H]
  initialize subset with first training example\;%
  \For{each training example}{%
    \eIf{the example is incorrectly classified by the KNN classifier
      using the current subset}{%
      add the current example to the subset\;%
    }{%
      do \emph{not} add the current example to the subset (do
      nothing)\; }%
  }%
  \caption{Condensed Nearest Neighbours}
\end{algorithm}

You are provided with an implementation of this \emph{condensed
  nearest neighbours} algorithm in \emph{knn.jl}.

\begin{enumerate}
\item%
  The point of this algorithm is to be faster than KNN. Try running
  the condensed nearest neighbours (called ``cknn'' in the code) on
  the \emph{citiesBig1} dataset and report how long it takes to make a
  prediction. What about if you try to use KNN for this dataset?

  \blu {CKNN takes approximately 0.4 milli-seconds to test each
    example (7 second for 14735 tests). KNN takes 5 milli-seconds to
    test each example (1minute and 31 seconds for 14735 tests ).}
  
\item%
  Report the training and testing errors for condensed NN, as well as
  the number of variables in the subset, on the \emph{citiesBig1}
  dataset with $k=1$.

  \blu%
  {Train Error with 1-nearest neighbours: 0.008 \\
    Test Error with 1-nearest neighbours: 0.018 \\
    There are 2 variables (features) in the subset }

\item%
  Why is the training error with $k=1$ now greater than $0$?
  
  \blu {Because we do not have all the examples in our subset which is
    only about 3\% of our dataset. This means that the nearest
    neighbour to some examples are no longer themselves as was the
    case with knn.}
  
\item%
  If you have $s$ examples in the subset, what is the cost of running the predict function on $t$ test examples in terms of $n$, $d$, $t$, and $s$?

  \blu {O(sdt)}  
\item%
  Try out your function on the dataset \emph{citiesBig2}. Why are the
  test error \emph{and} training error so high (even for $k=1$) for
  this method on this dataset?
  
  \blu{This is because citiesBig2 is a lot more stuctured than
    citiesBig1. For example, the first 1600 examples in citiesBig2 are
    all red (shown in Fig.~\ref{fig:cknn}a), where as in citiesBig1 we
    have nearly equal number of red and blue classes (shown in
    Fig.~\ref{fig:cknn}b). This structure causes Condenced Nearest
    neighbours to keep only a few (30) of the training examples in its
    condenced dataset because the majority of the the neighbouring
    points are close together in the dataset and will be classified
    correctly using the first point (which mean we will not include
    all the subsequent points of the same class in our condenced
    dataset). Since we keep only 30 of our examples from citiesBig2,
    compared to the 457 examples that we kept for citiesBig1, our
    training error suffers.}
\end{enumerate}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.45\linewidth]{img/frst1600Big2.png}  \hfil
  \includegraphics[width=0.45\linewidth]{img/first1600Big1.png} \\
  \makebox[0.45\linewidth][c]{(a) First 1600 data points in citiesBig2} \hfil
  \makebox[0.45\linewidth][c]{(b) First 1600 data points in citiesBig1}
  \caption{Figures for condensed $k$-NN}
  \label{fig:cknn}
\end{figure}

%% ------------------------------------------------------------------------- 
%%                        Very-Short Answer Questions
%% -------------------------------------------------------------------------

\section{Very-Short Answer Questions}

{Write a short one or two sentence answer to each of the questions
  below}. Make sure your answer is clear and concise.

\begin{enumerate}
\item%
  What is one reason we would want to look at scatterplots of the
  data before doing supervised learning?
  
  \blu{Scatterplots are helpful in spotting structured relationships
    between input variables. For example, we might observe that nearby
    points in our dataset tend to have the same label. This suggests
    that k-nearest neighbour might be a much better choice than Naive
    Bayes on that particular dataset. As another example, we can spot
    a correlation between one or more features and our labels which
    suggests those features to be more useful than others in our
    prediction task.}
  
\item%
  What is a reason that the examples in a training and test set
  might not be IID?

  \blu{One cause would be when the order of the objects matter or when
    they don't all come from the same distribution. For example, if we
    are measuring data about houses to predict their prices, and one
    neighbourhood has assisted housing while closeby areas don't have
    assisted housing (if we don't take that into account as a seperate
    feature), then the IID assumption breaks.}
  
\item%
  What is the difference between a validation set and a test set?

  \blu{Validation set is used to sqelect the best model or its
    hyper-parameters, where as, test set is used as an approximation
    of how the final model and its parameters (selected using
    validation sets) might perform on new examples. Basically,
    validation set is used to compare different models or parameters,
    while test set is used to approximate how well our final model
    generalizes.}
\item%
  Why is  naive Bayes called ``naive''?

  \blu{Because it assumes that all features $x_i(s)$ are conditionally
    independent given label $y_i$.}
  
\item%
  What is a situation where the naive Bayes assumption could lead
  to poor performance?
  
  \blu{One example would be when we have no spam messages with a given
    word which is used in an email. In this case p(a given word $|$
    spam) would be zero because we don't have any examples of it. This
    means that all the email with that given word will automatically
    get through our filter. In gerneral, the independence assumption
    made by Naive Bayes means that even when words are dependent, each
    word contributes evidence individually. Thus the magnitude of the
    weights for classes with strong word dependencies is larger than
    for classes with weak word dependencies. (words with strong
    dependency dominate the classification.}
  
\item%
  What is the main advantage of non-parametric models?

  \blu{The fact that in non-parametric models, the model becomes more
    complicated with more data means that we are less likely to suffer
    from a high bias problem. In Parametric models, eventually more
    data does not help because the model is too simple. In general,
    non-parametric models make fewer assumptions about the data and
    can perform better when the true distribution of data is unknown
    or cannot be easily approximated}
  
\item%
  A standard pre-processing step is ``standardization'' of the
  features: for each column of $X$ we subtract its mean and divide by
  its variance. Would this pre-processing change the accuracy of a
  decision tree classifier? Would it change the accuracy of a KNN
  classifier?

  \blu{This will probably NOT change the performance of the Decision
    Tree since we we are simply comparing a feature with a treshold
    value (to find the rule that best splits the data) and both the
    feature and the treshold are affected equaly by this
    pre-processing. However, KNN is highly affected by this
    pre-processing because it relies on Euclidean distances between
    example. This means that features with larger scales have larger
    differences and receive higher importance that features with
    smaller scales. This can be a big problem if features with smaller
    scales are more important in our classification task than features
    with larger scales.}
  
\item%
  Does increasing $k$ in KNN affect the training or prediction
  asymptotic runtimes?

  \blu{No it does not. There is not really a
    training step in KNN (we just remember the examples) and for
    prediction we have to calculate all the distances regardless. }
  
\item%
  How does increase the parameter $k$ in $k$-nearest neighbours
  affect the two parts (training error and approximation error) of the
  fundamental trade-off (hint: think of the extreme values).

  \blu{Increasing k increases the training error (at k=1 training
    error is 0) and decreases the approximation error (at $k = \infty$
    the approximation error approaches 0).}
  
\item%
  For any parametric model, how does increasing number of training
  examples $n$ affect the two parts of the fundamental trade-off.

  \blu{Increasing the number of training examples
    increases the training error and decreases the approximation error
    in parametric models.}  
\end{enumerate}

\end{document}