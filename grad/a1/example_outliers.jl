# Load X and y variable
using Statistics
using Printf
import JLD
import PyPlot
include("a1.jl")

function main()
    data = JLD.load("outliersData.jld")
    (X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

    # Fit a least squares model
    #model = a1.leastSquares(X,y)
    model = a1.leastAbsolutes(X,y)
    #model = a1.leastMax(X,y)

    # Evaluate training error
    yhat = model.predict(X)
    trainError = mean((yhat - y).^2)
    @printf("Squared train Error with least squares: %.3f\n",trainError)

    # Evaluate test error
    yhat = model.predict(Xtest)
    testError = mean((yhat - ytest).^2)
    @printf("Squared test Error with least squares: %.3f\n",testError)

    # Plot model
    PyPlot.figure()
    PyPlot.clf()
    PyPlot.plot(X,y,"b.")
    Xhat = minimum(X):.01:maximum(X)
    yhat = model.predict(Xhat)
    PyPlot.plot(Xhat,yhat,"g",linewidth=3)

    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.title("LeastAbsolute results")
    #PyPlot.title("LeastMax results")
    PyPlot.savefig("_saved_fig.pdf",bbox_inches="tight")
end


main()
